package app.dm.eventprogram_app.utiles

import android.content.Context
import android.net.ConnectivityManager

class Network {
    companion object{
        fun conexionInternet(activity: MyApp):Boolean{
            val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networwInfo = connectivityManager.activeNetworkInfo
            return networwInfo != null && networwInfo.isConnected
        }
    }
}