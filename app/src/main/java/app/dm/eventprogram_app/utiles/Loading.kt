@file:Suppress("DEPRECATION")
package app.dm.eventprogram_app.utiles

import android.app.ProgressDialog
import android.content.Context
import app.dm.eventprogram_app.R

object Loading {
    private var pd: ProgressDialog? = null
    fun ProgressDialog(
        context: Context?,
        title: String?,
        msg: String?,
        isCancel: Boolean
    ) {
        try {
            if (pd == null) {
                pd = ProgressDialog(context, R.style.ProgressBar)
                pd!!.setCancelable(isCancel)
                pd!!.setMessage(msg)
                pd!!.setTitle(title)
                pd!!.show()
            }
            if (!pd!!.isShowing) {
                pd!!.show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showProgressDialog(context: Context?) {
        ProgressDialog(context, null, "Cargando...", false)
    }

    fun removeProgressDialog() {
        try {
            if (pd != null) {
                if (pd!!.isShowing) {
                    pd!!.dismiss()
                    pd = null
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}