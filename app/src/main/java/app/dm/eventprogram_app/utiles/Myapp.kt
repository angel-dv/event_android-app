package app.dm.eventprogram_app.utiles

import android.app.Application

class MyApp: Application() {

    companion object {
        lateinit var instance: MyApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

//    override fun onCreate() {
//        super.onCreate()
//        instance = this
//    }

//    companion object {
//        lateinit var instance: MyApp
//            private set
//    }
}