package app.dm.eventprogram_app.utiles

class Constantes {

    companion object {
//        const val BASE_URL: String = "http://lugar1.zapto.org:8080/api/"
        //const val BASE_URL: String = "http://192.168.1.55:8080/api/"
        const val BASE_URL: String = "http://104.129.130.109/si-inblex/api/"

        const val TOPIC_EVENT = "TOPIC_EVENT"

        const val CONTENT_EVENT = "CONTENT_EVENT"
        const val DATE_EVENT = "DATE_EVENT"
        const val HOUR_EVENT = "HOUR_EVENT"
        const val ADD_NEW_EVENT = -1
        const val LOGIN_EMAIL: String = "LOGIN_EMAIL"

        const val LOGIN_PASS: String = "LOGIN_PASS"
        const val LOGIN_USERNAME: String = "LOGIN_USERNAME"
        const val LOGIN_ID: String = "LOGIN_ID"
        const val LOGIN_TOKEN: String = "LOGIN_TOKEN"
        const val ID_PROYECTO: String = "ID_PROYECTO"
            const val PROYECTO_COLOR: String = "PROYECTO_COLOR"

        const val ID_SPRINT: String = "ID_SPRINT"

        const val PROFILE_PERSON_ID: String = "PROFILE_PERSON_ID"
        const val PROFILE_PERSON_NOMBRE: String = "PROFILE_PERSON_NOMBRE"
        const val PROFILE_PERSON_APELLIDO: String = "PROFILE_PERSON_APELLIDO"
        const val PROFILE_PERSON_TELEFONO: String = "PROFILE_PERSON_TELEFONO"
        const val PROFILE_PERSON_CORREO: String = "PROFILE_PERSON_CORREO"
        const val PROFILE_PERSON_REGIMEN: String = "PROFILE_PERSON_REGIMEN"
        const val PROFILE_PERSON_RFC: String = "PROFILE_PERSON_RFC"
        const val PROFILE_PERSON_DOMICILIO_FISCAL: String = "PROFILE_PERSON_DOMICILIO_FISCAL"

        const val REFRESH_TOKEN: String = "REFRESH_TOKEN"
    }

}