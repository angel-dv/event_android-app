package app.dm.eventprogram_app.utiles

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesManager {
    companion object {
        private val APP_SETTINGS_FILE = "APP_SETTINGS"

        private fun SharedPreferencesManager() {}

        private fun getSharedPreferences(): SharedPreferences {
            return MyApp.instance.getSharedPreferences(APP_SETTINGS_FILE, Context.MODE_PRIVATE)
        }

        fun setStringValue(dataLabel: String?, dataValue: String?) {
            val editor = getSharedPreferences().edit()
            editor.putString(dataLabel, dataValue)
            editor.apply()
        }

        fun setIntValue(dataLabel: String?, dataValue: Int?){
            val editor = getSharedPreferences().edit()
            editor.putInt(dataLabel, dataValue!!)
            editor.apply()
        }

        fun setBoolValue(dataLabel: String?, dataValue: Boolean?){
            val editor = getSharedPreferences().edit()
            editor.putBoolean(dataLabel, dataValue!!)
            editor.apply()
        }

        fun getStringValue(dataLabel: String?): String? {
            return getSharedPreferences().getString(dataLabel, "")
        }

        fun getIntValue(dataLabel: String?): Int {
            return getSharedPreferences().getInt(dataLabel, -1)
        }

        fun getBoolValue(dataLabel: String?): Boolean {
            return getSharedPreferences().getBoolean(dataLabel, false)
        }

        fun removeDataValue(dataLabel: String?) {
            val editor = getSharedPreferences().edit()
            editor.remove(dataLabel)
            editor.apply()
        }
    }
}