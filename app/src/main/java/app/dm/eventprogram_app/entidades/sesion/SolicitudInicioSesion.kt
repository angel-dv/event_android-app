package app.dm.eventprogram_app.entidades.sesion


import com.google.gson.annotations.SerializedName

data class SolicitudInicioSesion(
    @SerializedName("username")
    val username: String,
    @SerializedName("password")
    val password: String
)