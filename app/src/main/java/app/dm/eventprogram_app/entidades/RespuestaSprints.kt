package app.dm.eventprogram_app.entidades

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RespuestaSprints (
    @Expose
    @SerializedName("duracion")
    val duracion: Int,

    @Expose
    @SerializedName("definicion")
    val definicion:String,

    @Expose
    @SerializedName("objetivos")
    val objetivos:RespuestaObjetivos,

    @Expose
    @SerializedName("fechaInicio")
    val fechaInicio:String,

    @Expose
    @SerializedName("fechaFin")
    val fechaFin:String,

    @Expose
    @SerializedName("id")
    val id: Long
)