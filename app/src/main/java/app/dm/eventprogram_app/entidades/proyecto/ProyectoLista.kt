package app.dm.eventprogram_app.entidades.proyecto

import com.google.gson.annotations.SerializedName

data class ProyectoLista(

    @SerializedName("id")
    val id: Int,
    @SerializedName("estado")
    val estado: Int,
    @SerializedName("fechaEstimada")
    val fechaEstimada: String,
    @SerializedName("progresoTotal")
    val progresoTotal: Int,
    @SerializedName("nombre")
    val nombre: String,
    var color: Int = 0
)