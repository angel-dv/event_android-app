package app.dm.eventprogram_app.entidades

import app.dm.eventprogram_app.R

class Proyecto {
    var id: Int = 0
    var nombre: String
    var descripcion: String
    var observaciones: String
    var fechaEstimada: String
    var imgProyecto: Int

    constructor(id: Int, nombre: String, descripcion: String, observaciones: String, fechaEstimada: String, estado: Int) {
        this.id = id
        this.nombre = nombre
        this.descripcion = descripcion
        this.observaciones = observaciones
        this.fechaEstimada = fechaEstimada
        if(estado == 1){
            imgProyecto = R.drawable.trabajando
        } else if(estado == 2){
            imgProyecto = R.drawable.hecho
        } else {
            imgProyecto = R.drawable.pausado
        }
    }


    fun GetNombre(): String{
        return nombre
    }

    fun GetDescripcion(): String{
        return descripcion
    }

    fun GetImgProyecto(): Int{
        return imgProyecto
    }

    fun GetFecha(): String{
        return fechaEstimada
    }
}