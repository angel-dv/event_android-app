package app.dm.eventprogram_app.entidades.registro

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

data class SolicitudRegistro (
    @SerializedName("nombre")
    val nombre: String,

    @SerializedName("apellido")
    val apellido: String,

    @SerializedName("telefono")
    val telefono: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("personaMoral")
    val personaMoral: Int,

    @SerializedName("rfc")
    val rfc: String,

    @SerializedName("domicilioFiscal")
    val domicilioFiscal: String,

    @SerializedName("password")
    val password: String


)

