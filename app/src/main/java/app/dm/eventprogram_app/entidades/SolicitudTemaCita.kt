package app.dm.eventprogram_app.entidades

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SolicitudTemaCita(
    @Expose
    @SerializedName("id") val id:Int
)