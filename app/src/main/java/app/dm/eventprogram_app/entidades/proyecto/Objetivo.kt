package app.dm.eventprogram_app.entidades.proyecto


import com.google.gson.annotations.SerializedName

data class Objetivo(
    @SerializedName("id")
    val id: Int,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("descripcion")
    val descripcion: String,
    @SerializedName("estado")
    val estado: Int
)