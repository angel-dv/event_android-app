package app.dm.eventprogram_app.entidades.proyecto


import com.google.gson.annotations.SerializedName

data class Sprint(
    @SerializedName("duracion")
    val duracion: Int,
    @SerializedName("definicion")
    val definicion: String,
    @SerializedName("objetivos")
    val objetivos: List<Objetivo>,
    @SerializedName("fechaInicio")
    val fechaInicio: String,
    @SerializedName("fechaFin")
    val fechaFin: String,
    @SerializedName("progreso")
    val progreso: Int,
    @SerializedName("id")
    val id: Int
)