package app.dm.eventprogram_app.entidades

import com.google.gson.annotations.SerializedName

class RespuestaEvento {
    @SerializedName("id")
    var id: Long? = null

    @SerializedName("contenido")
    var contenido: String? = null

    @SerializedName("fecha")
    var fecha: String? = null

    @SerializedName("hora")
    var hora: String? = null

    @SerializedName("temaCita")
    var temaCita: RespuestaTemaCita? = null

    @SerializedName("idTema")
    var idTema: Int? = null

    constructor() {}

    constructor(
        id: Long?,
        contenido: String?,
        fecha: String?,
        hora: String?,
        temaCita: RespuestaTemaCita?,
        idTema: Int?
    ) : super() {
        this.id = id
        this.contenido = contenido
        this.fecha = fecha
        this.hora = hora
        this.temaCita = temaCita
        this.idTema = idTema
    }

    constructor(nuevoEvento: RespuestaEvento) {
        id = nuevoEvento.id
        contenido = nuevoEvento.contenido
        fecha = nuevoEvento.fecha
        hora = nuevoEvento.hora
        temaCita = nuevoEvento.temaCita
        idTema = nuevoEvento.idTema
    }
}