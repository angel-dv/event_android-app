package app.dm.eventprogram_app.entidades.proyecto

import com.google.gson.annotations.SerializedName

data class RespuestaProgreso (
    @SerializedName("progresoTotal")
    val progresoTotal: Double,
    @SerializedName("progresoSprintActual")
    val progresoSprintActual: Double
)