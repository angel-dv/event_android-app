package app.dm.eventprogram_app.entidades.sesion


import com.google.gson.annotations.SerializedName

data class RespuestaInicioSesion(
    @SerializedName("mensaje")
    val mensaje: String,
    @SerializedName("token")
    val token: String,
    @SerializedName("user")
    val user: User
)