package app.dm.eventprogram_app.entidades.proyecto

import com.google.gson.annotations.SerializedName

//TODO: paso 2: crear la respuesta
data class RespuestaProyecto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("nombre")
    val nombre: String,
    @SerializedName("descripcion")
    val descripcion: String,
    @SerializedName("sprints")
    val sprints: List<Sprint>,
    @SerializedName("estado")
    val estado: Int,
    @SerializedName("fechaEstimada")
    val fechaEstimada: String,
    @SerializedName("progresoTotal")
    val progresoTotal: Int
)