package app.dm.eventprogram_app.entidades.editar

data class SolicitudEditarEvento(
    val id: Long?,
    val contenido: String,
    val fecha: String,
    val hora: String,
    val temaCita: TemaCita
)