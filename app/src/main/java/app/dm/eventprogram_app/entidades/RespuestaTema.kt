package app.dm.eventprogram_app.entidades

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RespuestaTema(
    @Expose
    @SerializedName("id")
    val id:Int,

    @Expose
    @SerializedName("tema")
    val tema:String
)