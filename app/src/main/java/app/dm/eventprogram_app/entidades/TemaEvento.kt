package app.dm.eventprogram_app.entidades

data class TemaEvento(
    var tema: String = "",
    var contenido: String = "",
    var fecha: String = "",
    var hora: String = "") {

//    init {
//        this.tema = tema
//        this.contenido = contenido
//        this.fecha = fecha
//        this.hora = hora
//    }
}