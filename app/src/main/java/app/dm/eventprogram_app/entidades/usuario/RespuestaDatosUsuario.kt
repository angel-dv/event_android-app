package app.dm.eventprogram_app.entidades.usuario

data class RespuestaDatosUsuario(
    val id: Int?,
    val nombre: String?,
    val apellido: String?,
    val telefono: String?,
    val email: String?,
    val personaMoral: Int?,
    val rfc: String?,
    val domicilioFiscal: String?,
    val password: String?
)