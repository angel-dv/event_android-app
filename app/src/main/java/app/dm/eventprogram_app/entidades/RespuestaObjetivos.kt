package app.dm.eventprogram_app.entidades

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RespuestaObjetivos (
    @Expose
    @SerializedName("id")
    val id:Long,

    @Expose
    @SerializedName("nombre")
    val nombre:String,

    @Expose
    @SerializedName("descripcion")
    val descripcion:String,

    @Expose
    @SerializedName("estado")
    val estado:Int
)