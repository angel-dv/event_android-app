package app.dm.eventprogram_app.entidades.sesion


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("accountNonExpired")
    val accountNonExpired: Boolean,
    @SerializedName("accountNonLocked")
    val accountNonLocked: Boolean,
    @SerializedName("authorities")
    val authorities: List<Any>,
    @SerializedName("credentialsNonExpired")
    val credentialsNonExpired: Boolean,
    @SerializedName("enabled")
    val enabled: Boolean,
    @SerializedName("id")
    val id: Int,
    @SerializedName("password")
    val password: String,
    @SerializedName("username")
    val username: String
)