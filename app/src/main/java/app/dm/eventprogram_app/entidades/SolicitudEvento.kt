package app.dm.eventprogram_app.entidades

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SolicitudEvento(
//    @Expose
//    @SerializedName("titulo")
//    val titulo:String,

    @Expose
    @SerializedName("contenido")
    val contenido:String,

    @Expose
    @SerializedName("fecha")
    val fecha:String,

    @Expose
    @SerializedName("hora")
    val hora:String,

    @Expose
    @SerializedName("temaCita")
    val temaCita:SolicitudTemaCita
)

//data class RequestTem(
//    @Expose
//    @SerializedName("id")
//    val id:Int
//)