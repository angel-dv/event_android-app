package app.dm.eventprogram_app.Adaptadores

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.proyecto.ProyectoLista
import app.dm.eventprogram_app.entidades.proyecto.RespuestaProyecto
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import java.util.*
import kotlin.collections.ArrayList

class AdaptadorProyectos: BaseAdapter {
    private lateinit var lista: List<ProyectoLista>
    private lateinit var contexto: Context
    lateinit var grafica: PieChart
    var otrocolor: Int = 0

    constructor(lista: ArrayList<ProyectoLista>, contexto: Context) : super() {
        this.lista = lista
        this.contexto = contexto
    }

    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View? {
        var item= lista.get(position)
        var convertView: View? = LayoutInflater.from(contexto).inflate(R.layout.forma_proyectos,null)

        val txtNombre: TextView = convertView!!.findViewById<TextView>(R.id.txtNombreProyecto)
        val txtEstado: TextView = convertView!!.findViewById<TextView>(R.id.txtEstadoProyecto)
        val txtFecha: TextView = convertView!!.findViewById<TextView>(R.id.txtFechaProyecto)
        var frame: FrameLayout = convertView!!.findViewById<FrameLayout>(R.id.inicioColor)
        grafica = convertView!!.findViewById<PieChart>(R.id.cuadroProy)

        txtNombre.setText(item.nombre)
        txtEstado.setText(getEstado(item.estado))
        txtEstado.setBackgroundResource(getColorEstado(item.estado))
        txtEstado.setTextColor(getColorTextoEstado(item.estado))
        txtFecha.setText(getFecha(item.fechaEstimada))
        val random = Random()
        val rnds = 1
        var colorFr = getColorFrame(rnds)
        item.color = rnds
        frame.setBackgroundResource(colorFr)
        AgregarDatos(item.progresoTotal)

        return convertView
    }

    fun getEstado(est: Int): String{
        var estado: String = ""
        when(est){
            0 -> estado = "En pausa"
            1 -> estado = "En desarrollo"
            2 -> estado = "Terminado"
            3 -> estado = "Cancelado"
        }
        return estado
    }

    fun getColorEstado(estado: Int): Int{
        var color: Int = 0
        when (estado){
            0 -> color = R.drawable.cuadro_amarillo
            1 -> color = R.drawable.cuadro_azul
            2 -> color = R.drawable.cuadro_verde
            3 -> color = R.drawable.cuadro_rojo

        }
        return color
    }

    fun getColorTextoEstado(estado: Int): Int{
        var color: Int = 0
        when (estado){
            0 -> color = R.color.colorAmarillo
            1 -> color = R.color.colorAzul
            2 -> color = R.color.colorVerde
            3 -> color = R.color.colorRojo

        }
        return color
    }

    fun getColorFrame(rnds: Int): Int{
        var color = 0
        when(rnds){
            1 -> {color = R.color.colorVerde
                otrocolor = -15476309}
            2 -> {color = R.color.colorAmarillo
                otrocolor = Color.YELLOW}
            3 -> {color = R.color.colorAzul
                otrocolor = Color.BLUE}
            4 -> {color = R.color.colorRojo
                otrocolor = Color.RED}
            5 -> {color = R.color.colorMorado
                otrocolor = -8252455}
            6 -> {color = R.color.colorRosa
                otrocolor = Color.MAGENTA}
            7 -> {color = R.color.colorCyan
                otrocolor = Color.CYAN}
        }
        return color
    }

    override fun getItem(p0: Int): Any {
        return lista.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return lista.size
    }

    fun setData(arrayProyectos: List<ProyectoLista>) {
        this.lista = arrayProyectos
        notifyDataSetChanged()
    }

    fun getColorView(it: Int): Int{
        return lista.get(it).color
    }

    fun getFecha(fechaInicio: String): String{
        var fi: String = ""
        var di = fechaInicio.substring(8)
        var mpi = fechaInicio.substring(5,7)
        var ai = fechaInicio.substring(0,4)
        var mi:String = ""
        when(mpi){
            "01" -> mi = "Enero"
            "02" -> mi = "Febrero"
            "03" -> mi = "Marzo"
            "04" -> mi = "Abril"
            "05" -> mi = "Mayo"
            "06" -> mi = "Junio"
            "07" -> mi = "Julio"
            "08" -> mi = "Agosto"
            "09" -> mi = "Septiembre"
            "10" -> mi = "Octubre"
            "11" -> mi = "Noviembre"
            "12" -> mi = "Diciembre"
        }
        fi = di + " de " + mi + " de " + ai


        return fi
    }

    private fun AgregarDatos(porc: Int){
        var datosY = FloatArray(2)
        var por: Float = porc.toString().toFloat()
        var dato: String = "$porc%"

        datosY[0] = por
        datosY[1] = 100 - datosY[0]
        val entradasY: ArrayList<PieEntry> = ArrayList()

        entradasY.add(PieEntry(datosY[0]))
        entradasY.add(PieEntry(datosY[1]))

        val pieDataSet: PieDataSet
        pieDataSet = PieDataSet(entradasY," ")
        pieDataSet.sliceSpace = 6F
        pieDataSet.valueTextSize = 12F
        grafica.centerText = dato
        grafica.setTouchEnabled(false)
        grafica.holeRadius = 70F



        val colores : ArrayList<Int> = ArrayList()
        colores.add(otrocolor)
        colores.add(Color.TRANSPARENT)
        pieDataSet.colors = colores

        val legend : Legend = grafica.legend
        legend.form = Legend.LegendForm.CIRCLE
        legend.setDrawInside(false)

        val pieData : PieData = PieData(pieDataSet)
        grafica.data = pieData
        grafica.invalidate()
        pieData.setDrawValues(false)
        grafica.description.isEnabled = false
        grafica.setDrawSliceText(false)
        grafica.legend.isEnabled = false

    }
}