package app.dm.eventprogram_app.Adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.proyecto.Objetivo

class AdaptadorObjetivos: BaseAdapter {
    private lateinit var lista: List<Objetivo>
    private lateinit var contexto: Context

    constructor(contexto: Context) : super() {
        this.contexto = contexto
    }


    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View? {
        var item: Objetivo = getItem(p0) as Objetivo
        var convertView: View? = LayoutInflater.from(contexto).inflate(R.layout.forma_objetivos,null)
        val imgFoto: ImageView = convertView!!.findViewById<ImageView>(R.id.imgObjetivo)
        val txtDescripcion: TextView = convertView!!.findViewById<TextView>(R.id.txtDescripcionObj)

        imgFoto.setImageResource(getIma(item.estado))
        txtDescripcion.setText(item.descripcion)
        return convertView
    }

    override fun getItem(p0: Int): Any {
        return lista.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return lista.size
    }

    fun getIma(estado: Int): Int{
        var imgAvance: Int = 0
        when(estado){
            0 -> imgAvance = R.drawable.ic_objetivo_en_pausa
            1 -> imgAvance = R.drawable.ic_objetivo_en_des
            2 -> imgAvance = R.drawable.ic_objetivo_listo
            3 -> imgAvance = R.drawable.ic_objetivo_cancelado
        }
        return imgAvance
    }

    fun setDatos(lista: List<Objetivo>){
        this.lista = lista
        notifyDataSetChanged()
    }
}