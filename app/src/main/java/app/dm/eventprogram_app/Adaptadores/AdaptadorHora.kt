package app.dm.eventprogram_app.Adaptadores

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CompoundButton
import android.widget.RadioButton
import android.widget.TextView
import app.dm.eventprogram_app.R

class AdaptadorHora: BaseAdapter {
    private var selectedAnswers: ArrayList<String>
    private var totalNumber: ArrayList<String>
    private var layoutInflater: LayoutInflater
    private var arrayItemId: ArrayList<Int>
    private var arrayHorario: ArrayList<String>
    private var arrayDisponibilidad: ArrayList<String>
    private var contexto: Context
//    private lateinit var dialogoHoraEventos: DialogoHoraEventos

    constructor(arrayItemId: ArrayList<Int>, arrayHorario: ArrayList<String>, arrayDisponibilidad: ArrayList<String>, contexto: Context) {
        this.arrayItemId = arrayItemId
        this.contexto = contexto
        this.arrayHorario = arrayHorario
        this.arrayDisponibilidad = arrayDisponibilidad

        selectedAnswers = ArrayList<String>()
        totalNumber = ArrayList<String>()
        for (i in arrayItemId.indices) {
            selectedAnswers.add("No ha seleccionado")
            totalNumber.add("No agrego el total")
        }

        layoutInflater = (LayoutInflater.from(contexto))
//        dialogoHoraEventos = DialogoHoraEventos(contexto)
    }

    override fun getView(position: Int, v: View?, viewGroup: ViewGroup?): View? {
        var v: View = layoutInflater.inflate(R.layout.item_hora_evento, null, false)

        var tvHora: TextView = v.findViewById(R.id.textViewItemHora)
        var rbDisponible: RadioButton = v.findViewById(R.id.radioButtonitemDisponible)

        if (arrayDisponibilidad.get(position).equals("No disponible")) {
            rbDisponible.isEnabled = false
            tvHora.isEnabled = false
            tvHora.setTextColor(Color.GRAY)
            tvHora.setText(arrayHorario.get(position))
            rbDisponible.setText(arrayDisponibilidad.get(position))
        } else {
            rbDisponible.isEnabled = true
            tvHora.isEnabled = true
            tvHora.setTextColor(Color.BLACK)
            tvHora.setText(arrayHorario.get(position))
            rbDisponible.setText(arrayDisponibilidad.get(position))
        }

        rbDisponible.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
//                selectedAnswers.set(position, arrayDisponibilidad.get(position))

//                DialogoClics.onCloseDialog(arrayHorario.get(position))
//                dialogoHoraEventos.ocultarDialogo()
            }
        })


        return v
    }

    override fun getItem(position: Int): Any {
        return arrayItemId.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return arrayItemId.count()
    }

}