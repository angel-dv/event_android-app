package app.dm.eventprogram_app.Adaptadores

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.RespuestaEvento
import app.dm.eventprogram_app.estructura.MyViewModel
import com.google.android.material.textfield.TextInputEditText
import java.util.*

class AdaptadorEventos : RecyclerView.Adapter<AdaptadorEventos.ViewHolder>() {

    var onItemClick: ((pos: Int, view: View) -> Unit)? = null
    var arrayEvent: ArrayList<RespuestaEvento>? = null
    var context: Context? = null
    var vieHolder: ViewHolder? = null
    lateinit var viewModel: MyViewModel

    fun isAllEvents(arrayEvent: ArrayList<RespuestaEvento>) {
        this.arrayEvent = arrayEvent
        notifyDataSetChanged()
    }

    fun isUpdate(context: Context?, arrayEvent: ArrayList<RespuestaEvento>) {
        this.arrayEvent = arrayEvent
        this.context = context
        viewModel =
            ViewModelProviders.of((context as FragmentActivity?)!!).get(MyViewModel::class.java)

        notifyDataSetChanged()
    }

    fun removerItem(position: Int) {
        arrayEvent!!.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_detalles_evento, parent, false)
        vieHolder = ViewHolder(v)
        return vieHolder!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val evento = arrayEvent?.get(position)

        if (evento?.fecha == null) {
            return
        }

        val auxDate: List<String> = evento?.fecha!!.split("-")
        val anio: String = auxDate[0]
        var mes = ""
        val dia: String = auxDate[2]

        when (auxDate[1]) {
            "01" -> mes = "Enero"
            "02" -> mes = "Febrero"
            "03" -> mes = "Marzo"
            "04" -> mes = "Abril"
            "05" -> mes = "Mayo"
            "06" -> mes = "Junio"
            "07" -> mes = "Julio"
            "08" -> mes = "Agosto"
            "09" -> mes = "Septiembre"
            "10" -> mes = "Octubre"
            "11" -> mes = "Noviembre"
            "12" -> mes = "Diciembre"
        }
        val auxFecha: String = dia + "\n" + mes + "\n" + anio
        val fecha = "$dia de $mes de $anio"

        holder.tveMes?.text = mes
        holder.tveDia?.text = dia
        holder.tveTema?.text = evento.temaCita?.tema
        holder.tveHora?.text = evento.hora?.substring(0, 5)
        holder.ibInfo?.setOnClickListener { v ->
            Toast.makeText(context, "Abrir informacion", Toast.LENGTH_SHORT).show()
        }

//        holder.ivEdit?.setOnClickListener {
////            dialogEdit(event, context)
//            viewModel.openDialogEdit(context, event!!)
//
//        }
    }

    override fun getItemCount(): Int {
        if (arrayEvent == null)
            return 0
        else return arrayEvent?.count()!!
    }

    private fun dialogEdit(
        event: RespuestaEvento?,
        context: Context?
    ) {
        val d = context?.let { Dialog(it) }
        d?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        d?.setCancelable(false)
        d?.setContentView(R.layout.dialog_event_edit)

        val tema: AutoCompleteTextView = d!!.findViewById(R.id.autoCompleteTextViewTopicEdit)
        val contenido: TextInputEditText = d!!.findViewById(R.id.textInpuEditTextContentEdit)
        val auxFecha: TextInputEditText = d!!.findViewById(R.id.textInputEditTextDateEdit)
        val hora: AutoCompleteTextView = d!!.findViewById(R.id.autoCompleteTextViewHourEdit)
        val confirmar: Button = d!!.findViewById(R.id.buttonEditOk)
        val cancelar: Button = d!!.findViewById(R.id.buttonEditCancel)

        val auxDate: List<String> = event?.fecha!!.split("-")
        val year: String = auxDate[0]
        var month = ""
        val day: String = auxDate[2]

        when (auxDate[1]) {
            "01" -> month = "Enero"
            "02" -> month = "Febrero"
            "03" -> month = "Marzo"
            "04" -> month = "Abril"
            "05" -> month = "Mayo"
            "06" -> month = "Junio"
            "07" -> month = "Julio"
            "08" -> month = "Agosto"
            "09" -> month = "Septiembre"
            "10" -> month = "Octubre"
            "11" -> month = "Noviembre"
            "12" -> month = "Diciembre"
        }
        val date = "$day de $month de $year"

        tema.setText(event?.temaCita?.tema.toString())
        contenido.setText(event?.contenido.toString())
        auxFecha.setText(date)
        hora.setText(event?.hora.toString())

        var eventPosition: Int
//        viewModel = ViewModelProviders.of((context as Fragment?)!!).get(MyViewModel::class.java)
//        viewModel.getAllTemas().observe((context as FragmentActivity?)!!, Observer {
        val temaList = listOf(
            "Costos y cotizaciones de un proyecto",
            "InformaciÃ³n sobre mi proyecto",
            "Cambios en mi proyecto",
            "Avances de mi proyecto",
            "Servicios de la empresa",
            "Otro"
        )

        val adaptador = ArrayAdapter(context, R.layout.item_list_select, temaList)
        tema.setAdapter(adaptador)
//        })

        tema.setOnItemClickListener { parent, view, position, id ->
            tema.setText(temaList.get(position))
//            eventPosition = listTema?.get(position).id!!
        }


        confirmar.setOnClickListener {
            Toast.makeText(this.context, "Aqui see mandara la solicitud", Toast.LENGTH_SHORT).show()
            d.dismiss()
        }
        cancelar.setOnClickListener {
            d.dismiss()
        }
        d.show()
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var tveMes: TextView? = null
        var tveDia: TextView? = null
        var tveTema: TextView? = null
        var tveHora: TextView? = null
        var ibInfo: ImageButton? = null

        init {
            tveMes = v.findViewById(R.id.textViewEventoMes)
            tveDia = v.findViewById(R.id.textViewEventoDia)
            tveTema = v.findViewById(R.id.textViewEventoTema)
            tveHora = v.findViewById(R.id.textViewEventoHora)
            ibInfo = v.findViewById(R.id.imageButtonInfo)

            v.setOnClickListener(this)
        }

        override fun onClick(position: View) {
            onItemClick?.invoke(adapterPosition, position)
        }
    }
}