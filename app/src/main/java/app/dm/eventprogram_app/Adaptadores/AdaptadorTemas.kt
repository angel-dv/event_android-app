package app.dm.eventprogram_app.Adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.RespuestaTema

class AdaptadorTemas(context: Context, items: List<RespuestaTema>): ArrayAdapter<RespuestaTema>(context, R.layout.item_list_select, items) {

    var items: List<RespuestaTema>? = null

    init {
        this.items = items
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var viewHolder: ViewHolder? = null
        var v: View? = convertView

        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.item_tema, null)
            viewHolder = ViewHolder(v)
            v.tag = viewHolder
        } else {
            viewHolder = v.tag as? ViewHolder
        }

        val item = getItem(position) as RespuestaTema
        viewHolder?.tvTema?.text = item.tema

        return v!!
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        var viewHolder: ViewHolder? = null
        var v: View? = convertView

        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.item_tema, null)
            viewHolder = ViewHolder(v)
            v.tag = viewHolder
        } else {
            viewHolder = v.tag as? ViewHolder
        }

        val item = getItem(position) as RespuestaTema
        viewHolder?.tvTema?.text = item.tema

        return v!!
    }

    override fun getItem(position: Int): RespuestaTema? {
        return this.items?.get(position)!!
    }

    override fun getCount(): Int {
        return this.items?.count()!!
    }


    private class ViewHolder(v: View) {
        var tvTema: TextView? = null

        init {
            tvTema = v.findViewById(R.id.textViewNombreTema)
        }
    }

}