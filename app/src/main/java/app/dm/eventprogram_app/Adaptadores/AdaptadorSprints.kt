package app.dm.eventprogram_app.Adaptadores

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.RespuestaSprints
import app.dm.eventprogram_app.entidades.proyecto.Sprint

class AdaptadorSprints: BaseAdapter {
    private lateinit var lista: List<Sprint>
    private lateinit var contexto: Context
    private var sprint: Int = 0
    private var color:Int = 0

    constructor(lista: List<Sprint>, contexto: Context, color: Int): super() {
        this.lista = lista
        this.contexto = contexto
        this.color = color
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View? {
        var item: Sprint = getItem(p0) as Sprint
        var convertView: View? = LayoutInflater.from(contexto).inflate(R.layout.forma_sprints, null)


        val txtId: TextView = convertView!!.findViewById<TextView>(R.id.txtNombreSprintLista)
        val txtDuracion: TextView = convertView!!.findViewById<TextView>(R.id.txtDuracionSprintLista)
        val circulo: ImageView = convertView!!.findViewById<ImageView>(R.id.imgSprint)
        val lineaArriba: View = convertView!!.findViewById<View>(R.id.lineaArriba)
        val lineaAbajo: View = convertView!!.findViewById<View>(R.id.lineaAbajo)

        sprint = p0 + 1
        txtId.setText("Sprint: " + sprint)
        txtDuracion.setText(getDuracion(item.fechaInicio, item.fechaFin))


        var par: Boolean = sprint%2 != 0


        circulo.setImageResource(getColorFrame(color,par))

        if(sprint == 1) lineaArriba.visibility = View.GONE

        if (sprint == getCount()) lineaAbajo.visibility = View.GONE


        return convertView
    }

    override fun getItem(p0: Int): Any {
        return lista.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return lista.size
    }

    fun getFondo(){

    }

    fun getDuracion(fechaInicio: String, fechaFin: String): String{
        var fi: String = ""
        var ff: String = ""
        var di = fechaInicio.substring(8)
        var df = fechaFin.substring(8)
        var mpi = fechaInicio.substring(5,7)
        var mpf = fechaFin.substring(5,7)
        var ai = fechaInicio.substring(0,4)
        var af = fechaFin.substring(0,4)
        var mi:String = ""
        var mf:String = ""
        when(mpi){
            "01" -> mi = "Enero"
            "02" -> mi = "Febrero"
            "03" -> mi = "Marzo"
            "04" -> mi = "Abril"
            "05" -> mi = "Mayo"
            "06" -> mi = "Junio"
            "07" -> mi = "Julio"
            "08" -> mi = "Agosto"
            "09" -> mi = "Septiembre"
            "10" -> mi = "Octubre"
            "11" -> mi = "Noviembre"
            "12" -> mi = "Diciembre"
        }

        when(mpf){
            "01" -> mf = "Enero"
            "02" -> mf = "Febrero"
            "03" -> mf = "Marzo"
            "04" -> mf = "Abril"
            "05" -> mf = "Mayo"
            "06" -> mf = "Junio"
            "07" -> mf = "Julio"
            "08" -> mf = "Agosto"
            "09" -> mf = "Septiembre"
            "10" -> mf = "Octubre"
            "11" -> mf = "Noviembre"
            "12" -> mf = "Diciembre"
        }
        if(ai == af){
            fi = di + " de " + mi
            ff = df + " de " + mf + " de " + ai
        } else {
            fi = di + " de " + mi + " de " + ai
            ff = df + " de " + mf + " de " + af
        }


        return fi + " - " + ff
    }

    fun getColorFrame(colorNum: Int,par: Boolean): Int{
        when(colorNum){
            1 ->{
                if(par) return R.drawable.circulo_verde
                else return R.drawable.circulo_verde2
            }
            2 ->{
                if(par) return R.drawable.circulo_amarillo
                else return R.drawable.circulo_amarillo2
            }
            3 ->{
                if(par) return R.drawable.circulo_azul
                else return R.drawable.circulo_azul2
            }
            4 ->{
                if(par) return R.drawable.circulo_rojo
                else return R.drawable.circulo_rojo2
            }
            5 ->{
                if(par) return R.drawable.circulo_negro
                else return R.drawable.circulo_negro2
            }
            6 ->{
                if(par) return R.drawable.circulo_rosa
                else return R.drawable.circulo_rosa2
            }
            7 ->{
                if(par) return R.drawable.circulo_cyan
                else return R.drawable.circulo_cyan2
            }

        }
        return 0
    }
}