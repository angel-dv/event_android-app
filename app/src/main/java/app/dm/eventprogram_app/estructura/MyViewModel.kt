package app.dm.eventprogram_app.estructura

import android.content.Context
import android.os.Handler
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import app.dm.eventprogram_app.entidades.RespuestaEvento
import app.dm.eventprogram_app.entidades.RespuestaTema
import app.dm.eventprogram_app.entidades.SolicitudEvento
import app.dm.eventprogram_app.entidades.editar.SolicitudEditarEvento
import app.dm.eventprogram_app.entidades.proyecto.ProyectoLista
import app.dm.eventprogram_app.entidades.proyecto.RespuestaProgreso
import app.dm.eventprogram_app.entidades.proyecto.RespuestaProyecto
import app.dm.eventprogram_app.entidades.proyecto.Sprint
import app.dm.eventprogram_app.entidades.usuario.RespuestaDatosUsuario
import app.dm.eventprogram_app.ui.DialogFragment

class MyViewModel : ViewModel() {
    private var countPress = 3
    private var repository: Repository
    private var allEventos: LiveData<ArrayList<RespuestaEvento>>
    private var allTemas: LiveData<List<RespuestaTema>>
    private var allProyectos: LiveData<List<ProyectoLista>>
    private lateinit var proyectoSel: LiveData<RespuestaProyecto>
    private lateinit var progresoProy: LiveData<RespuestaProgreso>
    private var datoUsuario: LiveData<RespuestaDatosUsuario>
    private lateinit var sprintSel: LiveData<Sprint>

    init {
        repository = Repository()
        allEventos = repository?.getEventos()!!
        allTemas = repository?.getTemas()!!
        allProyectos = repository?.getAllProyectos()!!
//        proyectoSel = repository?.getProyecto(1)!!
        datoUsuario = repository?.getDatoUsuarios()!!
    }

    fun getAllEventos(): LiveData<ArrayList<RespuestaEvento>> {
        return allEventos
    }

    fun getNewEvento(): LiveData<ArrayList<RespuestaEvento>> {
        allEventos = repository?.getEventos()!!
        return allEventos
    }

    fun insertEvent(requestEvent: SolicitudEvento) {
        repository.crearEvento(requestEvent)
    }

    fun getAllTemas(): LiveData<List<RespuestaTema>> {
        return allTemas
    }

    fun getAllProyectos(): LiveData<List<ProyectoLista>> {
        return allProyectos
    }

    fun eliminarEvento(idItem: Int){
        repository?.eliminarEvento(idItem)
    }

    fun openDialogEdit(context: Context?, event: RespuestaEvento) {
        if (countPress == 3) {
            val dialogEditEvent = DialogFragment(event)
            dialogEditEvent.show(
                (context as FragmentActivity?)!!.supportFragmentManager,
                "DialogFragment"
            )
            countPress = 2
        }

        val handler = Handler()
        val progressRun = Runnable {
            countPress = 3
        }
        handler.postDelayed(progressRun, 1000)
    }

    fun editarEvento(solicitudEditarEvento: SolicitudEditarEvento) {
        repository.editarEvento(solicitudEditarEvento)
    }

    fun getProyecto(id: Int): LiveData<RespuestaProyecto>{
        proyectoSel = repository?.getProyecto(id)!!
        return proyectoSel
    }

    fun getProgreso(id: Int): LiveData<RespuestaProgreso>{
        progresoProy = repository?.getProgreso(id)!!
        return progresoProy
    }

    fun getDatosUsuario(): LiveData<RespuestaDatosUsuario> {
        datoUsuario = repository?.getDatoUsuarios()!!
        return datoUsuario
    }

    fun getSprintSel(id: Int): LiveData<Sprint>{
        sprintSel = repository?.getSprintSeleccionado(id)!!
        return sprintSel
    }
}