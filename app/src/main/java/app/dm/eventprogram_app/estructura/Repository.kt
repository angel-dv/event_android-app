package app.dm.eventprogram_app.estructura

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import app.dm.eventprogram_app.api.ApiCliente
import app.dm.eventprogram_app.api.ApiServicio
import app.dm.eventprogram_app.entidades.*
import app.dm.eventprogram_app.entidades.editar.SolicitudEditarEvento
import app.dm.eventprogram_app.entidades.proyecto.ProyectoLista
import app.dm.eventprogram_app.entidades.proyecto.RespuestaProgreso
import app.dm.eventprogram_app.entidades.proyecto.RespuestaProyecto
import app.dm.eventprogram_app.entidades.proyecto.Sprint
import app.dm.eventprogram_app.entidades.usuario.RespuestaDatosUsuario
import app.dm.eventprogram_app.utiles.MyApp
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {
    private var apiServicio: ApiServicio? = null
    private var apiCliente: ApiCliente? = null
    private var todoEventos: MutableLiveData<ArrayList<RespuestaEvento>>? = null
    private var todoTema: MutableLiveData<List<RespuestaTema>>? = null
    private var todoProyectos: MutableLiveData<List<ProyectoLista>>? = null
    private var proyectoSel: MutableLiveData<RespuestaProyecto>? = null
    private var progresoProy: MutableLiveData<RespuestaProgreso>? = null
    private var datoUsuarios: MutableLiveData<RespuestaDatosUsuario>? = null
    private var sprintSel: MutableLiveData<Sprint>? = null

    init {
        apiCliente = ApiCliente.instance
        apiServicio = apiCliente?.getApiServicio()
        todoEventos = getEventos()
        todoTema = getTemas()
        todoProyectos = getAllProyectos()
        datoUsuarios = getDatoUsuarios()
    }

    fun getEventos(): MutableLiveData<ArrayList<RespuestaEvento>>? {
        if (todoEventos == null) {
            todoEventos = MutableLiveData<ArrayList<RespuestaEvento>>()
        }

        val call: Call<ArrayList<RespuestaEvento>>? = apiServicio?.getAllEvent()
        call?.enqueue(object : Callback<ArrayList<RespuestaEvento>> {
            override fun onFailure(call: Call<ArrayList<RespuestaEvento>>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(
                call: Call<ArrayList<RespuestaEvento>>,
                response: Response<ArrayList<RespuestaEvento>>
            ) {
                if (response.isSuccessful) {
                    todoEventos?.value = response.body()

                } else {
//                    Toast.makeText(
//                        MyApp.instance,
//                        "Disculpe las molestias, algo no ha salido bien. Por favor vuelva a intentarlo mas tarde",
//                        Toast.LENGTH_SHORT
//                    ).show()
                }
            }
        })

        return todoEventos
    }

    fun crearEvento(requestEvent: SolicitudEvento) {
        val call: Call<RespuestaEvento>? = apiServicio?.crearEvento(requestEvent)

        call?.enqueue(object : Callback<RespuestaEvento> {
            override fun onFailure(call: Call<RespuestaEvento>, t: Throwable) {
//                Toast.makeText(MyApp.instance, "Error en el servidor", Toast.LENGTH_SHORT).show()
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(
                call: Call<RespuestaEvento>,
                response: Response<RespuestaEvento>
            ) {
                if (response.isSuccessful) {
                    val listaClonada: ArrayList<RespuestaEvento> = ArrayList()
                    listaClonada.add(response.body()!!)
                    for (i in todoEventos?.value!!.indices) {
                        listaClonada.add(RespuestaEvento(todoEventos!!.value?.get(i)!!))
                    }

                    Log.d("TAG", listaClonada.toString())
                    todoEventos?.value = listaClonada
                } else {
                    Toast.makeText(
                        MyApp.instance,
                        "Disculpe las molestias, algo no ha salido bien. Por favor vuelva a intentarlo mas tarde",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        })
    }

    fun getTemas(): MutableLiveData<List<RespuestaTema>>? {
        if (todoTema == null) {
            todoTema = MutableLiveData<List<RespuestaTema>>()
        }

        val call: Call<List<RespuestaTema>>? = apiServicio?.obtenerTemas()
        call?.enqueue(object : Callback<List<RespuestaTema>> {
            override fun onFailure(call: Call<List<RespuestaTema>>, t: Throwable) {
//                Toast.makeText(MyApp.instance, "Error en el servidor", Toast.LENGTH_SHORT).show()
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(
                call: Call<List<RespuestaTema>>,
                response: Response<List<RespuestaTema>>
            ) {
                if (response.isSuccessful) {
                    todoTema?.value = response.body()
                } else {
                    Toast.makeText(
                        MyApp.instance,
                        "Disculpe las molestias, algo no ha salido bien. Por favor vuelva a intentarlo mas tarde",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })

        return todoTema
    }

    fun getAllProyectos(): MutableLiveData<List<ProyectoLista>>? {
        if (todoProyectos == null) {
            todoProyectos = MutableLiveData<List<ProyectoLista>>()
        }
        val call: Call<List<ProyectoLista>>? = apiServicio?.getAllProyectos()
        call?.enqueue(object : Callback<List<ProyectoLista>>{
            override fun onFailure(call: Call<List<ProyectoLista>>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(call: Call<List<ProyectoLista>>, response: Response<List<ProyectoLista>>) {
                if (response.isSuccessful) {
                    todoProyectos?.value = response.body()
                } else {
                    Log.d("TAG", response.message().toString())

//                    Toast.makeText(
//                        MyApp.instance,
//                        "Disculpe las molestias, algo no ha salido bien. Por favor vuelva a intentarlo mas tarde",
//                        Toast.LENGTH_SHORT
//                    ).show()
                }
            }
        })

        return todoProyectos
    }

    fun eliminarEvento(idItem: Int) {
        val call = apiServicio?.eliminarEvento(idItem)
        call?.enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    Log.d("TAG", response.body().toString())
                }
            }

        })

    }

    fun editarEvento(solicitudEditarEvento: SolicitudEditarEvento) {
        val call: Call<RespuestaEvento>? = apiServicio?.editarEvento(solicitudEditarEvento)
        call?.enqueue(object: Callback<RespuestaEvento>{
            override fun onFailure(call: Call<RespuestaEvento>, t: Throwable) {
                Log.d("TAG", t.toString())
            }

            override fun onResponse(call: Call<RespuestaEvento>, response: Response<RespuestaEvento>) {
                if (response.isSuccessful) {
                    val listaClonada: ArrayList<RespuestaEvento> = ArrayList()
                    listaClonada.add(response.body()!!)
                    for (i in todoEventos?.value!!.indices) {
                        listaClonada.add(RespuestaEvento(todoEventos!!.value?.get(i)!!))
                    }

                    Log.d("TAG", listaClonada.toString())
                    todoEventos?.value = listaClonada
                } else {
                    Toast.makeText(
                        MyApp.instance,
                        "Disculpe las molestias, algo no ha salido bien. Por favor vuelva a intentarlo mas tarde",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        })
    }

    fun getProyecto(id: Int): MutableLiveData<RespuestaProyecto>? {
        if(proyectoSel == null){
            proyectoSel = MutableLiveData<RespuestaProyecto>()
        }

        val call: Call<RespuestaProyecto>? = apiServicio?.getProyecto(id)
        call?.enqueue(object: Callback<RespuestaProyecto>{
            override fun onFailure(call: Call<RespuestaProyecto>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(
                call: Call<RespuestaProyecto>,
                response: Response<RespuestaProyecto>
            ) {
                if (response.isSuccessful) {
                    proyectoSel?.value = response.body()
                } else {
//                    Toast.makeText(
//                        MyApp.instance,
//                        "Disculpe las molestias, algo no ha salido bien. Por favor vuelva a intentarlo mas tarde",
//                        Toast.LENGTH_SHORT
//                    ).show()
                }
            }

        })

        return proyectoSel
    }

    fun getProgreso(id: Int): MutableLiveData<RespuestaProgreso>? {
        if(progresoProy == null){
            progresoProy = MutableLiveData<RespuestaProgreso>()
        }

        val call: Call<RespuestaProgreso>? = apiServicio?.getProgresos(id)
        call?.enqueue(object : Callback<RespuestaProgreso>{
            override fun onFailure(call: Call<RespuestaProgreso>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(
                call: Call<RespuestaProgreso>,
                response: Response<RespuestaProgreso>
            ) {
                if(response.isSuccessful){
                    progresoProy?.value = response.body()
                } else{
//                    Toast.makeText(
//                        MyApp.instance,
//                        "Disculpe las molestias, algo no ha salido bien. Por favor vuelva a intentarlo mas tarde",
//                        Toast.LENGTH_SHORT
//                    ).show()
                }
            }

        })

        return progresoProy
    }

    fun getDatoUsuarios(): MutableLiveData<RespuestaDatosUsuario>? {
        if (datoUsuarios == null) {
            datoUsuarios = MutableLiveData<RespuestaDatosUsuario>()
        }

        val call: Call<RespuestaDatosUsuario>? = apiServicio?.obtenerDatoUsuario()
        call?.enqueue(object: Callback<RespuestaDatosUsuario>{
            override fun onFailure(call: Call<RespuestaDatosUsuario>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(call: Call<RespuestaDatosUsuario>, response: Response<RespuestaDatosUsuario>) {
                if (response.isSuccessful) {
                    datoUsuarios?.value = response.body()
                } else {
                    Log.d("TAG", response.body().toString())
                }
            }

        })

        return datoUsuarios
    }

    fun getSprintSeleccionado(id: Int): MutableLiveData<Sprint>?{
        if(sprintSel == null){
            sprintSel = MutableLiveData<Sprint>()
        }

        val call: Call<Sprint>? = apiServicio?.getSprint(id)
        call?.enqueue(object: Callback<Sprint>{
            override fun onFailure(call: Call<Sprint>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }

            override fun onResponse(call: Call<Sprint>, response: Response<Sprint>) {
                if(response.isSuccessful){
                    sprintSel?.value = response.body()
                } else {
                    Log.d("TAG", response.body().toString())
                }
            }

        })

        return sprintSel
    }

}
