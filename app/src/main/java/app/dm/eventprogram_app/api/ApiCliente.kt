package app.dm.eventprogram_app.api

import app.dm.eventprogram_app.utiles.Constantes
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiCliente {
    private val apiServicio: ApiServicio
    private val retrofit: Retrofit

    companion object {
        var instance: ApiCliente? = null
            get() {
            if (field == null){
                instance = ApiCliente()
            }
            return field
        }
    }

    init {
        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.addInterceptor(Interceptor())
        val cliente = okHttpClientBuilder.build()

        retrofit = Retrofit.Builder()
            .baseUrl(Constantes.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(cliente)
            .build()

        apiServicio = retrofit.create(ApiServicio::class.java)
    }

    fun getApiServicio() = apiServicio
}