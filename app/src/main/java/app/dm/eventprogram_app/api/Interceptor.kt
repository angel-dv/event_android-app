package app.dm.eventprogram_app.api

import android.os.AsyncTask
import android.util.Log
import app.dm.eventprogram_app.utiles.Constantes
import app.dm.eventprogram_app.utiles.SharedPreferencesManager
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class Interceptor: Interceptor {
    private var refreshToken = false

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = SharedPreferencesManager.getStringValue(Constantes.LOGIN_TOKEN)

        val original = chain.request()
        val request = original.newBuilder()
            .header("Authorization", "Bearer $token")
            .method(original.method, original.body)
            .build()
        val response = chain.proceed(request)
        Log.d("INTERCEPTOR-01", "Code : " + response.code)
        if (response.code == 403) {
            Log.d("INTERCEPTOR-02", "Ya caduco el token, Code:" + response.code)
            refreshToken = true
            refreshTokenAsyncTask().execute(refreshToken)
            return response
        }
        return response
    }

    private class refreshTokenAsyncTask : AsyncTask<Boolean?, Void?, Void?>() {

        override fun doInBackground(vararg booleans: Boolean?): Void? {
            Log.d("refreshTokenAsyncTask", booleans.toString())
            SharedPreferencesManager.setBoolValue(Constantes.REFRESH_TOKEN, booleans[0])
            return null
        }
    }

}