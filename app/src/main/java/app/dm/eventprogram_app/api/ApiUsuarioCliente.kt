package app.dm.eventprogram_app.api

import app.dm.eventprogram_app.utiles.Constantes
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiUsuarioCliente {
    private val apiUsuarioServicio: ApiUsuarioServicio
    private val retrofit: Retrofit

    companion object {
        var instance: ApiUsuarioCliente? = null
            get() {
                if (field == null){
                    instance = ApiUsuarioCliente()
                }
                return field
            }
    }

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(Constantes.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiUsuarioServicio = retrofit.create(ApiUsuarioServicio::class.java)
    }

    fun getApiUsuarioServicio() = apiUsuarioServicio

}