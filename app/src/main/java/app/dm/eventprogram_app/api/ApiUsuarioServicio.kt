package app.dm.eventprogram_app.api

import app.dm.eventprogram_app.entidades.registro.RespuestaRegistro
import app.dm.eventprogram_app.entidades.registro.SolicitudRegistro
import app.dm.eventprogram_app.entidades.sesion.RespuestaInicioSesion
import app.dm.eventprogram_app.entidades.sesion.SolicitudInicioSesion
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiUsuarioServicio {

    @POST("login")
    fun inicioSesion(@Body solicitudInicioSesion: SolicitudInicioSesion): Call<RespuestaInicioSesion>

    @POST("clientes")
    fun registro(@Body solicitudRegistro: SolicitudRegistro): Call<RespuestaRegistro>

}