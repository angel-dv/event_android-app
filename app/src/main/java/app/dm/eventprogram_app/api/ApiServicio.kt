package app.dm.eventprogram_app.api

import app.dm.eventprogram_app.entidades.RespuestaEvento
import app.dm.eventprogram_app.entidades.RespuestaTema
import app.dm.eventprogram_app.entidades.SolicitudEvento
import app.dm.eventprogram_app.entidades.editar.SolicitudEditarEvento
import app.dm.eventprogram_app.entidades.proyecto.ProyectoLista
import app.dm.eventprogram_app.entidades.proyecto.RespuestaProgreso
import app.dm.eventprogram_app.entidades.proyecto.RespuestaProyecto
import app.dm.eventprogram_app.entidades.proyecto.Sprint
import app.dm.eventprogram_app.entidades.usuario.RespuestaDatosUsuario
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiServicio {

    @GET("agenda/cita")
    fun getAllEvent(): Call<ArrayList<RespuestaEvento>>

    @POST("agenda/cita")
    fun crearEvento(@Body solicitudEvento: SolicitudEvento): Call<RespuestaEvento>

    @GET("agenda/cita/temas")
    fun obtenerTemas(): Call<List<RespuestaTema>>

    @GET("proyectos")
    fun getAllProyectos(): Call<List<ProyectoLista>>

    @GET("proyectos/show/{id}")
    fun getProyecto(@Path("id") id: Int): Call<RespuestaProyecto>

    @GET("proyectos/progresos/{id}")
    fun getProgresos(@Path("id") id: Int): Call<RespuestaProgreso>

    @DELETE("agenda/cita/{id}")
    fun eliminarEvento(@Path("id") itemEvento: Int): Call<ResponseBody>

    @PUT("agenda/cita")
    fun editarEvento(@Body solicitudEditarEvento: SolicitudEditarEvento): Call<RespuestaEvento>

    @GET("clientes/show")
    fun obtenerDatoUsuario(): Call<RespuestaDatosUsuario>

    @GET("proyectos/sprints/show/{id}")
    fun getSprint(@Path("id") id: Int): Call<Sprint>
}