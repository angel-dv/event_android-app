package app.dm.eventprogram_app.ui

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import java.util.*

class FechaDialogFragment : DialogFragment() {
    private var listener: DatePickerDialog.OnDateSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c: Calendar = Calendar.getInstance()
        val year: Int = c.get(Calendar.YEAR)
        val month: Int = c.get(Calendar.MONTH)
        val day: Int = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(activity!!, listener, year, month, day)

        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000

        return datePickerDialog
    }

    companion object {
        fun newInstance(listener: DatePickerDialog.OnDateSetListener): FechaDialogFragment {
            val fragment = FechaDialogFragment()
            fragment.listener = listener
            return fragment
        }

    }

}

