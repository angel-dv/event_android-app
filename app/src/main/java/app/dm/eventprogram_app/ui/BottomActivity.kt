package app.dm.eventprogram_app.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.utiles.Constantes
import app.dm.eventprogram_app.utiles.ProyectoS
import app.dm.eventprogram_app.utiles.SharedPreferencesManager
import com.google.android.material.bottomnavigation.BottomNavigationView


class BottomActivity : AppCompatActivity(),
    ProyectoS {

    private var nav: BottomNavigationView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom)

        nav = findViewById(R.id.navigation)

        openNavigationView()
        initFragment()
        //openFragment(ListaSprints())
//        nav?.selectedItemId = R.id.itemHome
    }

    private fun openNavigationView() {
        nav?.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.itemHome -> {
                    val f = HomeFragment.newInstance("HomeFragment")
                    openFragment(f)
                    true
                }
                R.id.itemEvent -> {
                    val f = EventFragment.newInstance("EventFragment")
                    openFragment(f)
                    true
                }
                R.id.itemProfile -> {
                    val f = ProfileFragment.newInstance("ProfileFragment")
                    openFragment(f)
                    true
                }
                else -> false
            }
        }
    }

    private fun initFragment() {
        val manager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = manager.beginTransaction()
        transaction.add(R.id.fragmentContainer, HomeFragment.newInstance("HomeFragment"))
        transaction.commit()
    }

    private fun openFragment(f: Fragment) {
        val manager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = manager.beginTransaction()
        transaction.add(R.id.fragmentContainer, f)
        transaction.commit()
    }

    private fun AbrirFragment(f: Fragment) {
        val manager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = manager.beginTransaction()
        transaction.add(R.id.fragmentContainer, f)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun ProyectoSeleccionado() {
        AbrirFragment(ListaSprints())

    }

    override fun onResume() {
        super.onResume()
        val h = Handler()
        val r = Runnable {
            if (SharedPreferencesManager.getBoolValue(Constantes.REFRESH_TOKEN)) {
                confirmSignOff()
            }
        }
        h.postDelayed(r, 2000)
    }

    private fun confirmSignOff() {
        val builder =
            AlertDialog.Builder(this)
        builder.setCancelable(false)
            .setTitle("Cerrando sesión por seguridad")
            .setMessage("Por favor, vuelva a inciar sesión, el token a expirado")
            .setPositiveButton(
                "Cerrar sesión"
            ) { dialog, id ->
                SharedPreferencesManager.removeDataValue(Constantes.LOGIN_PASS)
                SharedPreferencesManager.removeDataValue(Constantes.LOGIN_EMAIL)
                SharedPreferencesManager.removeDataValue(Constantes.LOGIN_USERNAME)
                SharedPreferencesManager.removeDataValue(Constantes.LOGIN_ID)
                SharedPreferencesManager.removeDataValue(Constantes.LOGIN_TOKEN)

                SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_ID)
                SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_NOMBRE)
                SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_CORREO)
                SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_TELEFONO)
                SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_REGIMEN)
                SharedPreferencesManager.removeDataValue(Constantes.REFRESH_TOKEN)

                val i = Intent(this, InicioSesion::class.java)
                startActivity(i)
                finish()
                dialog.dismiss()
            }
        val dialog = builder.create()
        dialog.show()
    }


}
