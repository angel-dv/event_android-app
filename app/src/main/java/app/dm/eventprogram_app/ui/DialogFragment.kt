package app.dm.eventprogram_app.ui

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import app.dm.eventprogram_app.Adaptadores.AdaptadorTemas
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.RespuestaEvento
import app.dm.eventprogram_app.entidades.RespuestaTema
import app.dm.eventprogram_app.entidades.editar.SolicitudEditarEvento
import app.dm.eventprogram_app.entidades.editar.TemaCita
import app.dm.eventprogram_app.estructura.MyViewModel
import com.google.android.material.textfield.TextInputEditText

class DialogFragment(event: RespuestaEvento) : DialogFragment() {

    var respuestaEvento: RespuestaEvento? = null
    var atvTema: AutoCompleteTextView? = null
    var etDescripcion: TextInputEditText? = null
    var etFecha: TextInputEditText? = null
    var atvHora: AutoCompleteTextView? = null
    lateinit var viewModel: MyViewModel
    var adaptador: AdaptadorTemas? = null
    var listTema: List<RespuestaTema>? = null
    var eventPosition: Int? = null
    var auxDate: String? = null

    init {
        this.respuestaEvento = event
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppThemeMaterialDesignDialog)
        viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val v: View = inflater.inflate(R.layout.dialog_event_edit, container, false)
        getDialog()?.getWindow()
            ?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        val etTitulo = v.findViewById<TextView>(R.id.textViewDialogDateEdit)
        atvTema = v.findViewById(R.id.autoCompleteTextViewTopicEdit)
        etDescripcion = v.findViewById(R.id.textInpuEditTextContentEdit)
        etFecha = v.findViewById(R.id.textInputEditTextDateEdit)
        atvHora = v.findViewById(R.id.autoCompleteTextViewHourEdit)
        val btnOk = v.findViewById<Button>(R.id.buttonEditOk)
        val btnCancel = v.findViewById<Button>(R.id.buttonEditCancel)

        listTema = ArrayList()

        viewModel.getAllTemas().observe(activity!!, Observer {
            listTema = it
            adaptador = AdaptadorTemas(activity!!, listTema!!)
            atvTema?.setAdapter(adaptador)
        })

        atvTema?.setOnItemClickListener { parent, view, position, id ->
            atvTema?.setText(listTema?.get(position)?.tema)
            eventPosition = listTema?.get(position)?.id
        }

        val id = respuestaEvento?.id
        val idTemaCita = respuestaEvento?.temaCita?.id
        val tema = respuestaEvento?.temaCita?.tema
        val descripcion = respuestaEvento?.contenido
        val fecha = respuestaEvento?.fecha
        var hora = respuestaEvento?.hora
        val idTema = respuestaEvento?.idTema

        hora = hora?.substring(0, hora.length - 3)

        atvTema?.setText(tema.toString())
        etDescripcion?.setText(descripcion.toString())
        etDescripcion?.setSelection(etDescripcion?.text!!.length)
        etFecha?.setText(fecha.toString())
        atvHora?.setText(hora.toString())

        val itemsHour = listOf(
            "10:00",
            "11:00",
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00"
        )

        val adapterHour = ArrayAdapter(activity!!, R.layout.item_list_select, itemsHour)
        atvHora?.setAdapter(adapterHour)

        etFecha?.setOnClickListener {
            showDatePickerDialog()
        }

        btnOk.setOnClickListener {
            showDialogConfirm(id!!, idTemaCita)
            dialog?.dismiss()
        }

        btnCancel.setOnClickListener {
            dialog?.dismiss()
        }

        return v
    }

    private fun showDatePickerDialog() {
        val newFragment =
            FechaDialogFragment.newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
                val dayStr = day
                val monthStr = (month + 1)

                val selectedDate = "$year-$monthStr-$dayStr"
                auxDate = selectedDate

                val auxDate: List<String> = selectedDate.split("-")
                val auxYear: String = auxDate[0]
                var auxMonth = ""
                val auxDay: String = auxDate[2]

                when (auxDate[1]) {
                    "1" -> auxMonth = "Enero"
                    "2" -> auxMonth = "Febrero"
                    "3" -> auxMonth = "Marzo"
                    "4" -> auxMonth = "Abril"
                    "5" -> auxMonth = "Mayo"
                    "6" -> auxMonth = "Junio"
                    "7" -> auxMonth = "Julio"
                    "8" -> auxMonth = "Agosto"
                    "9" -> auxMonth = "Septiembre"
                    "10" -> auxMonth = "Octubre"
                    "11" -> auxMonth = "Noviembre"
                    "12" -> auxMonth = "Diciembre"
                }
                val date = "$auxDay de $auxMonth de $auxYear"

                etFecha?.setText(date)
            })

        newFragment.show(activity!!.supportFragmentManager, "datePicker")
    }

    private fun showDialogConfirm(id: Long, idTemaCita: Int?) {
        val topic = atvTema?.text.toString()
        val content = etDescripcion?.text.toString()
        val fecha = etFecha?.text.toString()
        var date = auxDate
        val hour = atvHora?.text.toString()

        if (date == null) {
            date = fecha
        }

        if (eventPosition == null) {
            eventPosition = idTemaCita
        }

        if (!topic.isEmpty() && !content.isEmpty() && !date!!.isEmpty() && !hour.isEmpty()) {
            editDialog(id, content, date, hour, eventPosition!!)
        } else {
            Toast.makeText(activity, "Por favor llene todo los campos", Toast.LENGTH_SHORT).show()
        }
    }

    private fun editDialog(
        idEvento: Long,
        content: String,
        date: String,
        hour: String,
        eventPosition: Int
    ) {
        val builder = AlertDialog.Builder(activity!!)

        builder.setMessage("¿Desea editar el evento?")
            .setTitle("Editar evento en agenda")

        builder.setPositiveButton("Confirmar") { dialog, id ->
            val temaCita = TemaCita(eventPosition)
            val solicitudEditarEvento =
                SolicitudEditarEvento(idEvento, content, date, hour, temaCita)

            viewModel.editarEvento(solicitudEditarEvento)
            dialog.dismiss()
        }

        builder.setNegativeButton("Cancelar") { dialog, id ->
            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.show()

    }

}