package app.dm.eventprogram_app.ui

import android.os.Bundle
import android.view.KeyEvent
import android.widget.CheckBox
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import app.dm.eventprogram_app.R

class Registro : AppCompatActivity() {
    private var cbCompany: CheckBox? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        cbCompany = findViewById(R.id.radioButtonCompany)

        replaceFragment(Personas())
        cbCompany?.setOnClickListener {
            if (cbCompany!!.isChecked) {
                replaceFragment(Empresas())
            } else {
                replaceFragment(Personas())
            }
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == event!!.keyCode) {
            this.finish()
        }
        return super.onKeyDown(keyCode, event)
    }
}
