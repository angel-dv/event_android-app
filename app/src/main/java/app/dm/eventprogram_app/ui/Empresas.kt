package app.dm.eventprogram_app.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.api.ApiUsuarioCliente
import app.dm.eventprogram_app.api.ApiUsuarioServicio
import app.dm.eventprogram_app.entidades.registro.RespuestaRegistro
import app.dm.eventprogram_app.entidades.registro.SolicitudRegistro
import app.dm.eventprogram_app.utiles.Loading
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Empresas : Fragment() {

    private var apiUsuarioCliente: ApiUsuarioCliente? = null
    private var apiUsuarioServicio: ApiUsuarioServicio? = null
    private var eteRepresentante: EditText? = null
    private var eteRFC: EditText? = null
    private var eteCorreo: EditText? = null
    private var eteDomicilio: EditText? = null
    private var eteTelefono: EditText? = null
    private var eteContrasenia: EditText? = null
    private var eteConfContrasenia: EditText? = null
    private var btneRegistrar: Button? = null
    private var tvInicioSesion: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_empresas, container, false)
        initRetrofit()
        findViews(v)

        btneRegistrar?.setOnClickListener {
            val nombre: String = eteRepresentante?.text.toString()
            var rfc: String = eteRFC?.text.toString()
            val correo = eteCorreo?.text.toString()
            var domicilio: String = eteDomicilio?.text.toString()
            val telefono = eteTelefono?.text.toString()
            val contrasenia = eteContrasenia?.text.toString()
            val confContrasenia = eteConfContrasenia?.text.toString()
            if (nombre.isNotEmpty() && telefono.isNotEmpty() && correo.isNotEmpty() && contrasenia.isNotEmpty() && confContrasenia.isNotEmpty()) {
                if (contrasenia.equals(confContrasenia)) {
                    if (rfc.isEmpty()) rfc = " "
                    if (domicilio.isEmpty()) domicilio = " "
                    registrarEmpresa(nombre, rfc, domicilio, telefono, correo, contrasenia)
                } else {
                    eteConfContrasenia?.setError("Las contraseñas no coinciden")
                }
            } else {
                if (nombre.isEmpty()) eteRepresentante?.setError("El nombre o razón social es necesario")
                if (correo.isEmpty()) eteCorreo?.setError("El correo electrónico es necesario")
                if (telefono.isEmpty()) eteTelefono?.setError("El teléfono es necesario")
                if (contrasenia.isEmpty()) eteContrasenia?.setError("Por favor, escriba una contraseña")
                if (confContrasenia.isEmpty()) eteConfContrasenia?.setError("Por favor, confirme su contraseña")
            }

        }

        tvInicioSesion?.setOnClickListener {
            activity!!.finish()
        }

        return v
    }

    private fun findViews(v: View) {
        eteRepresentante = v.findViewById(R.id.editTextEmpresaNombre)
        eteRFC = v.findViewById(R.id.editTextEmpresaRFC)
        eteCorreo = v.findViewById(R.id.editTextEmpresaCorreo)
        eteDomicilio = v.findViewById(R.id.editTextEmpresaDomicilio)
        eteTelefono = v.findViewById(R.id.editTextEmpresaTelefono)
        eteContrasenia = v.findViewById(R.id.editTextEmpresaContrasenia)
        eteConfContrasenia = v.findViewById(R.id.editTextEmpresaConfContrasenia)
        btneRegistrar = v.findViewById(R.id.buttonEmpresaRegistrar)
        tvInicioSesion = v.findViewById(R.id.textViewEmpresaIniciarSesion)
    }

    private fun initRetrofit() {
        apiUsuarioCliente = ApiUsuarioCliente.instance
        apiUsuarioServicio = apiUsuarioCliente?.getApiUsuarioServicio()
    }

    private fun registrarEmpresa(
        nombre: String,
        rfc: String,
        domicilio: String,
        telefono: String,
        correo: String,
        contra: String
    ) {
        Loading.showProgressDialog(context)
        val registro = SolicitudRegistro(nombre, " ", telefono, correo, 1, rfc, domicilio, contra)

        val call: Call<RespuestaRegistro>? = apiUsuarioServicio?.registro(registro)

        call?.enqueue(object : Callback<RespuestaRegistro> {
            override fun onFailure(call: Call<RespuestaRegistro>, t: Throwable) {
                Log.d("TAG-Login", t.message.toString())
                Toast.makeText(
                    context,
                    "Error al conectar con el servidor\n\nPor favor intente mas tarde",
                    Toast.LENGTH_LONG
                ).show()
                Loading.removeProgressDialog()

            }

            override fun onResponse(
                call: Call<RespuestaRegistro>,
                response: Response<RespuestaRegistro>
            ) {
                if (response.isSuccessful) {
                    Toast.makeText(
                        context,
                        "Empresa registrada",
                        Toast.LENGTH_SHORT
                    ).show()
                    Loading.removeProgressDialog()
                    vaciar()
                } else {
                    Log.d("TAG_registro", response.body().toString())
                    Loading.removeProgressDialog()
                    Toast.makeText(
                        context,
                        "Lo sentimos, ha habido un problema con su registro, intente mas tarde",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    fun vaciar() {
        eteRepresentante?.setText("")
        eteRFC?.setText("")
        eteCorreo?.setText("")
        eteDomicilio?.setText("")
        eteTelefono?.setText("")
        eteContrasenia?.setText("")
        eteConfContrasenia?.setText("")
    }

}
