package app.dm.eventprogram_app.ui

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.Color.BLACK
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.dm.eventprogram_app.Adaptadores.AdaptadorEventos
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.RespuestaEvento
import app.dm.eventprogram_app.estructura.MyViewModel
import app.dm.eventprogram_app.utiles.MyApp
import app.dm.eventprogram_app.utiles.Network
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.dialog_event.view.*

private const val ARG_PARAM1 = "param1"

class EventFragment : Fragment() {
    private lateinit var viewModel: MyViewModel
    private var param1: String? = null
    private var fab: FloatingActionButton? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var arrayEvent: ArrayList<RespuestaEvento> = ArrayList()
    private var refresh: SwipeRefreshLayout? = null
    private var rvEvent: RecyclerView? = null
    private var tvEmpty: TextView? = null
    private var adapter: AdaptadorEventos? = null
    private var itemTouchHelper: ItemTouchHelper? = null
    private var coutDialog = 0
    private lateinit var dialogoDetalleEventos: DialogoDetalleEventos

    companion object {
        fun newInstance(param1: String) = EventFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, param1)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        viewModel = ViewModelProviders.of(activity as FragmentActivity).get(MyViewModel::class.java)
        viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_event, container, false)

        dialogoDetalleEventos = DialogoDetalleEventos(context!!)

        val context: Context = v.context
        fab = v.findViewById(R.id.fab)
        refresh = v.findViewById(R.id.swipeRefreshLayoutEvent)
        rvEvent = v.findViewById(R.id.recyclerViewEvent)
        tvEmpty = v.findViewById(R.id.textViewEmpty)

        allDataModelView(context)
        return v
    }

    private fun allDataModelView(context: Context) {
        rvEvent?.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(activity)
        rvEvent?.layoutManager = layoutManager
//        adapter = AdapterEvent(arrayEvent!!)
        adapter = AdaptadorEventos()
        rvEvent?.adapter = adapter

        if (Network.conexionInternet(MyApp.instance)) {
            viewModel.getAllEventos().observe(viewLifecycleOwner, Observer { event ->
                arrayEvent = event
                if (!event.isNullOrEmpty()) {
                    adapter?.isUpdate(activity!!, arrayEvent)
                    isVisibility(true, "")
                } else {
                    isVisibility(false, "")
                }
            })
        } else {
            val mensaje = "Requiere una conexión a internet,\n Por favor vuelva a intentarlo.."
            isVisibility(false, mensaje)
        }

        adapter?.onItemClick = { position, v ->
            if (coutDialog == 0) {
                coutDialog = 1
                val event = arrayEvent!!.get(position)
                val tema = event.temaCita!!.tema
                val contenido = event.contenido!!
//                val fecha = event.fecha
                val hora = event.hora!!

                var date = ""
                if (event.fecha == null) {
                    date = "01 de Enero de 2018"
                } else {
                    val auxDate: List<String> = event.fecha!!.split("-")
                    val year: String = auxDate[0]
                    var month = ""
                    val day: String = auxDate[2]

                    when (auxDate[1]) {
                        "01" -> month = "Enero"
                        "02" -> month = "Febrero"
                        "03" -> month = "Marzo"
                        "04" -> month = "Abril"
                        "05" -> month = "Mayo"
                        "06" -> month = "Junio"
                        "07" -> month = "Julio"
                        "08" -> month = "Agosto"
                        "09" -> month = "Septiembre"
                        "10" -> month = "Octubre"
                        "11" -> month = "Noviembre"
                        "12" -> month = "Diciembre"
                    }
                    date = "$day de $month de $year"
                }

                var dialogoEvento = false
                dialogoDetalleEventos.setDatosEvento(tema, date, hora, contenido, "Pendiente")

                if (!dialogoEvento) {
                    dialogoDetalleEventos.mostrarDialogoEvento()
                    dialogoEvento = true
                }

                coutDialog = DialogoClics.onClickDialog(0)

//                goToDialog(tema, contenido, date, hora)

            }
        }

        fab?.setOnClickListener {
            val i = Intent(activity, EventoActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            startActivity(i)
        }

        refresh?.setOnRefreshListener {
            isUpdateModelView()
        }

        itemHelper()

    }

    private fun itemHelper() {
        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                removerItemDialog(position)
            }

            @RequiresApi(Build.VERSION_CODES.M)
            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    val p = Paint()
                    p.color = resources.getColor(android.R.color.holo_red_dark, null)

                    val iconDelete: Bitmap;

                    val itemView = viewHolder.itemView
                    val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                    val width = height / 3
                    if (dX > 0) {
                        val bg = RectF(
                            itemView.left.toFloat(),
                            itemView.top.toFloat(),
                            dX,
                            itemView.bottom.toFloat()
                        )
                        c.drawRect(bg, p)

                        val iconPosition = RectF(
                            itemView.left.toFloat() + width,
                            itemView.top.toFloat() + width,
                            itemView.left.toFloat() + 2 * width,
                            itemView.bottom.toFloat() - width
                        )
                        iconDelete = BitmapFactory.decodeResource(
                            resources,
                            android.R.drawable.ic_menu_delete
                        )
                        c.drawBitmap(iconDelete, null, iconPosition, p)
                    } else {
                        val background = RectF(
                            itemView.right.toFloat() + dX,
                            itemView.top.toFloat(),
                            itemView.right.toFloat(),
                            itemView.bottom.toFloat()
                        )
                        c.drawRect(background, p)

                        val iconPosition = RectF(
                            itemView.right.toFloat() - 2 * width,
                            itemView.top.toFloat() + width,
                            itemView.right.toFloat() - width,
                            itemView.bottom.toFloat() - width
                        )
                        iconDelete = BitmapFactory.decodeResource(
                            resources,
                            android.R.drawable.ic_menu_delete
                        )
                        c.drawBitmap(iconDelete, null, iconPosition, p)
                    }
                }
                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }
        }).attachToRecyclerView(rvEvent)
    }

    private fun removerItemDialog(position: Int) {
        val alertDialog: AlertDialog? = activity.let {
            val builder = AlertDialog.Builder(it!!)
            builder.setCancelable(false)
            builder.setTitle("Eliminar evento")
            builder.setMessage("¿Desea realmente eliminar el evento?")
            builder.apply {
                setPositiveButton("Confirmar") { dialog, id ->
                    val id: Int = arrayEvent.get(position).id!!.toInt()
                    viewModel.eliminarEvento(id)
                    adapter?.removerItem(position)

                    val handler = Handler()
                    val progressRun = Runnable {
                        isUpdateModelView()
                        dialog.dismiss()
                    }
                    handler.postDelayed(progressRun, 1000)
                    Toast.makeText(activity, "Evento eliminado correctamente", Toast.LENGTH_SHORT)
                        .show()
                }
                setNegativeButton("Cancelar") { dialog, id ->
                    isUpdateModelView()
                    dialog.dismiss()
                }
            }
            builder.create()
        }
        alertDialog?.show()
            .apply {
                alertDialog?.getButton(AlertDialog.BUTTON_POSITIVE)?.let {
                    it.setTextColor(BLACK)
                    it.setBackgroundColor(resources.getColor(android.R.color.white))
                }
                alertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)?.let {
                    it.setTextColor(BLACK)
                    it.setBackgroundColor(resources.getColor(android.R.color.white))
                }
            }
    }

    private fun goToDialog(tema: String, contenido: String, fecha: String, hora: String) {
        val dialogBuilder = activity?.let { AlertDialog.Builder(it) }
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_event, null)
        dialogBuilder?.setCancelable(false)

        dialogBuilder?.setView(dialogView)

        dialogView.textViewDialogTopic.text = tema
        dialogView.textViewDialogContent.text = contenido
        dialogView.textViewDialogDate.text = fecha
        dialogView.textViewDialogHour.text = hora

        dialogBuilder?.setPositiveButton("Aceptar") { dialog, id ->
            dialog.dismiss()
            coutDialog = 0
        }
        dialogBuilder?.setNegativeButton("Cancelar") { dialog, which ->
            dialog?.dismiss()
            coutDialog = 0
        }

        val alertDialog = dialogBuilder?.create()
        alertDialog?.show()
    }

    private fun isUpdateModelView() {
        if (Network.conexionInternet(MyApp.instance)) {
            val eventObserver = Observer<ArrayList<RespuestaEvento>> {
                if (!it.isNullOrEmpty()) {
                    arrayEvent = it
                    refresh?.isRefreshing = false
                    adapter?.isAllEvents(arrayEvent)
                    isVisibility(true, "")
                } else {
                    isVisibility(false, "")
                }
                viewModel.getNewEvento().removeObservers(requireActivity())
            }
            viewModel.getNewEvento().observe(requireActivity(), eventObserver)
        } else {
            Toast.makeText(
                activity,
                "Requiere una conexión a internet, Por favor vuelva a intentarlo..",
                Toast.LENGTH_SHORT
            ).show()
        }

        val h = Handler()
        val r = Runnable {
            refresh?.isRefreshing = false
        }
        h.postDelayed(r, 4000)
    }

    override fun onStart() {
        super.onStart()
        val eventObserver = Observer<ArrayList<RespuestaEvento>> {
            arrayEvent = it
            adapter?.isUpdate(activity!!, arrayEvent)
            viewModel.getNewEvento().removeObservers(requireActivity())
        }
        viewModel.getNewEvento().observe(requireActivity(), eventObserver)
    }

    private fun isVisibility(isChange: Boolean, mensaje: String) {
        if (isChange) {
            refresh?.visibility = View.VISIBLE
            rvEvent?.visibility = View.VISIBLE
            tvEmpty?.visibility = View.GONE
        } else {
            refresh?.visibility = View.GONE
            rvEvent?.visibility = View.GONE
            tvEmpty?.visibility = View.VISIBLE

            if (mensaje.isNotEmpty()) {
                tvEmpty?.text = mensaje
            }
        }
    }
}
