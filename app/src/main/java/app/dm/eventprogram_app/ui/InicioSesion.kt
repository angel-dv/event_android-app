package app.dm.eventprogram_app.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.api.ApiUsuarioCliente
import app.dm.eventprogram_app.api.ApiUsuarioServicio
import app.dm.eventprogram_app.entidades.sesion.RespuestaInicioSesion
import app.dm.eventprogram_app.entidades.sesion.SolicitudInicioSesion
import app.dm.eventprogram_app.utiles.Constantes
import app.dm.eventprogram_app.utiles.Loading
import app.dm.eventprogram_app.utiles.SharedPreferencesManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InicioSesion : AppCompatActivity() {
    private var apiUsuarioCliente: ApiUsuarioCliente? = null
    private var apiUsuarioServicio: ApiUsuarioServicio? = null
    private var etCorreo: EditText? = null
    private var etContrasenia: EditText? = null
    private var tvRecuContrasenia: TextView? = null
    private var btnIniciar: TextView? = null
    private var tvRegistro: TextView? = null
    private var correo: String? = null
    private var contrasenia: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppThemeLogin)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inicio_sesion)
        initRetrofit()
        findViews()

        btnIniciar?.setOnClickListener {
            correo = etCorreo?.text.toString()
            contrasenia = etContrasenia?.text.toString()

            if (correo!!.isEmpty()) {
                etCorreo?.setError("El correo electrónico es requerido")
            } else if (contrasenia!!.isEmpty()) {
                etContrasenia?.setError("La contraseña es requerida")
            } else {
                Loading.showProgressDialog(this)
                val datosUsuario = SolicitudInicioSesion(correo!!, contrasenia!!)

                val call: Call<RespuestaInicioSesion>? =
                    apiUsuarioServicio?.inicioSesion(datosUsuario)
                call?.enqueue(object : Callback<RespuestaInicioSesion> {
                    override fun onFailure(call: Call<RespuestaInicioSesion>, t: Throwable) {
                        Log.d("TAG-Login", t.message.toString())
                        Toast.makeText(
                            applicationContext,
                            "Error al conectar con en servidor\n\nPor favor intente mas tarde",
                            Toast.LENGTH_SHORT
                        ).show()
                        Loading.removeProgressDialog()
                    }

                    override fun onResponse(
                        call: Call<RespuestaInicioSesion>,
                        response: Response<RespuestaInicioSesion>
                    ) {
                        if (response.isSuccessful) {
                            Log.d("TAG-Login", response.toString())

                            SharedPreferencesManager.setStringValue(
                                Constantes.LOGIN_EMAIL,
                                correo
                            )
                            SharedPreferencesManager.setStringValue(
                                Constantes.LOGIN_PASS,
                                contrasenia
                            )

                            SharedPreferencesManager.setStringValue(
                                Constantes.LOGIN_USERNAME,
                                response.body()!!.user.username
                            )
                            SharedPreferencesManager.setIntValue(
                                Constantes.LOGIN_ID,
                                response.body()!!.user.id
                            )
                            SharedPreferencesManager.setStringValue(
                                Constantes.LOGIN_TOKEN,
                                response.body()!!.token
                            )

                            irInicio()
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Usuario inexistente ó no reconocido\n\nVerifique sus datos",
                                Toast.LENGTH_SHORT
                            ).show()
                            Loading.removeProgressDialog()
                        }
                    }
                })
            }
        }

        tvRecuContrasenia?.setOnClickListener {
            Toast.makeText(
                this,
                "Opción en proceso, Por favor inténtelo más tarde",
                Toast.LENGTH_SHORT
            ).show()
        }

        tvRegistro?.setOnClickListener {
            val j = Intent(this, Registro::class.java)
            startActivity(j)
        }
    }

    private fun findViews() {
        etCorreo = findViewById(R.id.editTextLoginEmail)
        etContrasenia = findViewById(R.id.editTextLoginPassword)
        tvRecuContrasenia = findViewById(R.id.textViewLoginForgotPassword)
        btnIniciar = findViewById(R.id.buttonLoginStart)
        tvRegistro = findViewById(R.id.textViewLoginRegister)
    }

    private fun initRetrofit() {
        apiUsuarioCliente = ApiUsuarioCliente.instance
        apiUsuarioServicio = apiUsuarioCliente?.getApiUsuarioServicio()
    }

    override fun onStart() {
        super.onStart()
        try {
            val correo = SharedPreferencesManager.getStringValue(Constantes.LOGIN_EMAIL)
            val contrasena = SharedPreferencesManager.getStringValue(Constantes.LOGIN_PASS)

            if (!correo!!.isEmpty() && !contrasena!!.isEmpty()) {
                irInicio()
            }
        } catch (e: Exception) {
        }
    }

    private fun irInicio() {
        val i = Intent(applicationContext, BottomActivity::class.java)
        startActivity(i)
        Loading.removeProgressDialog()
        finish()
    }
}
