package app.dm.eventprogram_app.ui

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.ListView
import android.widget.TextView
import app.dm.eventprogram_app.Adaptadores.AdaptadorObjetivos
import app.dm.eventprogram_app.R

class DialogoObjetivoSprint {
    private lateinit var nombreSprint: TextView
    private lateinit var duracionSprint: TextView
    private lateinit var listaObjetivosSprint: ListView
    private lateinit var porcentajeSprint: TextView
    private lateinit var aceptar: TextView
    private lateinit var dialogo: Dialog

    constructor(contexto: Context) {
        dialogo = Dialog(contexto)
        dialogo.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogo.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        dialogo.setCancelable(false)
        dialogo.setContentView(R.layout.dialogo_objetivo_sprint)

        nombreSprint = dialogo.findViewById(R.id.txtNombreSprint)
        duracionSprint = dialogo.findViewById(R.id.txtDuracionSprint)
        listaObjetivosSprint = dialogo.findViewById(R.id.listaObjetivosSprint)
        porcentajeSprint = dialogo.findViewById(R.id.txtPorcentajeSprint)
        aceptar = dialogo.findViewById(R.id.txtAceptar)


        aceptar.setOnClickListener {
            dialogo.dismiss()
        }
    }

    fun setDatos(
        nombre: String,
        duracion: String,
        adaptador: AdaptadorObjetivos,
        porcentaje: String
    ) {
        nombreSprint.setText(nombre)
        duracionSprint.setText(duracion)
        listaObjetivosSprint.adapter = adaptador
        porcentajeSprint.setText(porcentaje)
        listaObjetivosSprint.divider = null
    }

    fun mostrarDialogo() {
        dialogo.show()
    }
}