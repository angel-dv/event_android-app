package app.dm.eventprogram_app.ui

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import app.dm.eventprogram_app.Adaptadores.AdaptadorObjetivos
import app.dm.eventprogram_app.Adaptadores.AdaptadorSprints
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.proyecto.Objetivo
import app.dm.eventprogram_app.entidades.proyecto.RespuestaProyecto
import app.dm.eventprogram_app.entidades.proyecto.Sprint
import app.dm.eventprogram_app.estructura.MyViewModel
import app.dm.eventprogram_app.utiles.*
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry

/**
 * A simple [Fragment] subclass.
 */
class ListaSprints : Fragment(), AdapterView.OnItemClickListener {

    lateinit var lista: ListView
    lateinit var txtDescripcion: TextView
    lateinit var txtEstado: TextView
    lateinit var proyecto: RespuestaProyecto
    lateinit var sprint: Sprint
    var sprints: List<Sprint> = ArrayList()
    private lateinit var viewModel: MyViewModel
    private lateinit var titulo: TextView
    lateinit var grafica: PieChart
    lateinit var txtFecha: TextView
    lateinit var txtFecha2: TextView
    lateinit var cuadroFecha: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Loading.showProgressDialog(context)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var frag = inflater.inflate(R.layout.fragment_lista_sprints, container, false)
        viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

        lista = frag.findViewById<ListView>(R.id.listaSprint)
        titulo = frag.findViewById(R.id.txtProyectoTitulo)
        txtEstado = frag.findViewById(R.id.txtEstadoProy)
        txtDescripcion = frag.findViewById(R.id.txtDescripcionProy)
        grafica = frag.findViewById(R.id.graficaProyecto)
        txtFecha = frag.findViewById(R.id.txtFecha1)
        txtFecha2 = frag.findViewById(R.id.txtFecha2)
        cuadroFecha = frag.findViewById(R.id.frameFecha)

        var idProyecto = SharedPreferencesManager.getIntValue(Constantes.ID_PROYECTO)

        if (Network.conexionInternet(MyApp.instance)) {
            viewModel?.getProyecto(idProyecto).observe(viewLifecycleOwner, Observer {
                proyecto = it
                sprints = proyecto.sprints
                titulo.setText(proyecto.nombre)
                txtEstado.setTextColor(getColorTextoEstado(proyecto.estado))
                txtEstado.setBackgroundResource(getColorEstado(proyecto.estado))
                txtEstado.setText(getEstado(proyecto.estado))
                txtDescripcion.text = proyecto.descripcion
                val colorCir = SharedPreferencesManager.getIntValue(Constantes.PROYECTO_COLOR)
                val adaptadorSprints = AdaptadorSprints(sprints, frag.context, colorCir)
                val colorGrafica = getColorGrafica(colorCir)
                AgregarDatos(proyecto.progresoTotal, colorGrafica)
                txtFecha.setText(getFecha(proyecto.fechaEstimada))
                cuadroFecha.setBackgroundResource(getFondoFecha(colorCir))
                if (colorCir == 5) {
                    txtFecha.setTextColor(-1)
                    txtFecha2.setTextColor(-1)
                }
                lista.adapter = adaptadorSprints
                lista.divider = null
                Loading.removeProgressDialog()

            })
            /*viewModel?.getProgreso(idProyecto).observe(viewLifecycleOwner, Observer {
                progreso = it
                porciento = progreso.progresoTotal
                porciento2 = progreso.progresoSprintActual
                AgregarDatos(porciento,"Proyecto")
            })*/
        } else {
            Loading.removeProgressDialog()
            Toast.makeText(
                activity,
                "Requiere una conexión a internet,\n Por favor vuelva a intentarlo..",
                Toast.LENGTH_SHORT
            ).show()
        }




        lista.setOnItemClickListener(this@ListaSprints)

        return frag
    }

    fun getFecha(fecha: String): String {
        var fi: String = ""
        var di = fecha.substring(8)
        var mpi = fecha.substring(5, 7)
        var ai = fecha.substring(0, 4)
        var mi: String = ""
        when (mpi) {
            "01" -> mi = "Enero"
            "02" -> mi = "Febrero"
            "03" -> mi = "Marzo"
            "04" -> mi = "Abril"
            "05" -> mi = "Mayo"
            "06" -> mi = "Junio"
            "07" -> mi = "Julio"
            "08" -> mi = "Agosto"
            "09" -> mi = "Septiembre"
            "10" -> mi = "Octubre"
            "11" -> mi = "Noviembre"
            "12" -> mi = "Diciembre"
        }
        fi = di + " de " + mi + " de " + ai


        return fi
    }

    fun getEstado(est: Int): String {
        var estado: String = ""
        when (est) {
            0 -> estado = "En pausa"
            1 -> estado = "En desarrollo"
            2 -> estado = "Terminado"
            3 -> estado = "Cancelado"
        }
        return estado
    }

    fun getColorEstado(estado: Int): Int {
        var color: Int = 0
        when (estado) {
            0 -> color = R.drawable.cuadro_amarillo
            1 -> color = R.drawable.cuadro_azul
            2 -> color = R.drawable.cuadro_verde
            3 -> color = R.drawable.cuadro_rojo

        }
        return color
    }

    fun getColorTextoEstado(estado: Int): Int {
        var color: Int = 0
        when (estado) {
            0 -> color = R.color.colorAmarillo
            1 -> color = R.color.colorAzul
            2 -> color = R.color.colorVerde
            3 -> color = R.color.colorRojo

        }
        return color
    }

    fun getColorGrafica(rnds: Int): Int {
        var color = 0
        when (rnds) {
            1 -> color = -15476309
            2 -> color = Color.YELLOW
            3 -> color = Color.BLUE
            4 -> color = Color.RED
            5 -> color = Color.BLACK
            6 -> color = Color.MAGENTA
            7 -> color = Color.CYAN
        }
        return color
    }

    fun getFondoFecha(rnds: Int): Int {
        var color = 0
        when (rnds) {
            1 -> color = R.drawable.cuadroverde
            2 -> color = R.drawable.cuadroamarillo
            3 -> color = R.drawable.cuadroazul
            4 -> color = R.drawable.cuadrorojo
            5 -> color = R.drawable.cuadronegro
            6 -> color = R.drawable.cuadrorosa
            7 -> color = R.drawable.cuadrocyan
        }
        return color
    }

    private fun AgregarDatos(porc: Int, otrocolor: Int) {
        var datosY = FloatArray(2)
        var por: Float = porc.toString().toFloat()
        var dato: String = "$porc%"

        datosY[0] = por
        datosY[1] = 100 - datosY[0]
        val entradasY: ArrayList<PieEntry> = ArrayList()

        entradasY.add(PieEntry(datosY[0]))
        entradasY.add(PieEntry(datosY[1]))

        val pieDataSet: PieDataSet
        pieDataSet = PieDataSet(entradasY, " ")
        pieDataSet.sliceSpace = 2F
        pieDataSet.valueTextSize = 30F

        grafica.centerText = dato
        grafica.setCenterTextSize(32F)
        grafica.setTouchEnabled(false)
        grafica.holeRadius = 80F


        val colores: ArrayList<Int> = ArrayList()
        colores.add(otrocolor)
        colores.add(Color.TRANSPARENT)
        pieDataSet.colors = colores

        val legend: Legend = grafica.legend
        legend.form = Legend.LegendForm.CIRCLE
        legend.setDrawInside(false)

        val pieData: PieData = PieData(pieDataSet)
        grafica.data = pieData
        grafica.invalidate()
        pieData.setDrawValues(false)
        grafica.description.isEnabled = false
        grafica.setDrawSliceText(false)
        grafica.legend.isEnabled = false

    }

    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        var estaAc: Activity = requireActivity()
        Loading.showProgressDialog(estaAc)
        val item = p0?.adapter?.getItem(p2) as Sprint
        val id = item.id
        var ya = false
        var objetivos: List<Objetivo> = ArrayList()
        var adap: AdaptadorObjetivos = AdaptadorObjetivos(estaAc)
        val dia = DialogoObjetivoSprint(estaAc)
        var nombre = ""
        var duracion = ""
        var porcentaje = ""
        if (Network.conexionInternet(MyApp.instance)) {
            viewModel?.getSprintSel(id).observe(viewLifecycleOwner, Observer {
                sprint = it
                objetivos = sprint.objetivos
                nombre = sprint.definicion
                duracion = getDuracion(sprint.fechaInicio, sprint.fechaFin)
                adap.setDatos(objetivos)
                porcentaje = "${sprint.progreso}%"
                dia.setDatos(nombre, duracion, adap, porcentaje)
                if (!ya) {
                    dia.mostrarDialogo()
                    ya = true
                }

            })
        } else {
            Toast.makeText(
                activity,
                "Requiere una conexión a internet,\n Por favor vuelva a intentarlo..",
                Toast.LENGTH_SHORT
            ).show()
        }
        Loading.removeProgressDialog()


    }

    fun getDuracion(fechaInicio: String, fechaFin: String): String {
        var fi: String = ""
        var ff: String = ""
        var di = fechaInicio.substring(8)
        var df = fechaFin.substring(8)
        var mpi = fechaInicio.substring(5, 7)
        var mpf = fechaFin.substring(5, 7)
        var ai = fechaInicio.substring(0, 4)
        var af = fechaFin.substring(0, 4)
        var mi: String = ""
        var mf: String = ""
        when (mpi) {
            "01" -> mi = "Enero"
            "02" -> mi = "Febrero"
            "03" -> mi = "Marzo"
            "04" -> mi = "Abril"
            "05" -> mi = "Mayo"
            "06" -> mi = "Junio"
            "07" -> mi = "Julio"
            "08" -> mi = "Agosto"
            "09" -> mi = "Septiembre"
            "10" -> mi = "Octubre"
            "11" -> mi = "Noviembre"
            "12" -> mi = "Diciembre"
        }

        when (mpf) {
            "01" -> mf = "Enero"
            "02" -> mf = "Febrero"
            "03" -> mf = "Marzo"
            "04" -> mf = "Abril"
            "05" -> mf = "Mayo"
            "06" -> mf = "Junio"
            "07" -> mf = "Julio"
            "08" -> mf = "Agosto"
            "09" -> mf = "Septiembre"
            "10" -> mf = "Octubre"
            "11" -> mf = "Noviembre"
            "12" -> mf = "Diciembre"
        }
        if (ai == af) {
            fi = di + " de " + mi
            ff = df + " de " + mf + " de " + ai
        } else {
            fi = di + " de " + mi + " de " + ai
            ff = df + " de " + mf + " de " + af
        }


        return fi + " - " + ff
    }


}
