package app.dm.eventprogram_app.ui

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.graphics.Color
import android.graphics.Color.BLACK
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.ViewModelProviders
import app.dm.eventprogram_app.Adaptadores.AdaptadorHora
import app.dm.eventprogram_app.Adaptadores.AdaptadorTemas
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.RespuestaTema
import app.dm.eventprogram_app.entidades.SolicitudEvento
import app.dm.eventprogram_app.entidades.SolicitudTemaCita
import app.dm.eventprogram_app.estructura.MyViewModel
import app.dm.eventprogram_app.utiles.MyApp
import app.dm.eventprogram_app.utiles.Network
import com.google.android.material.textfield.TextInputEditText
import de.hdodenhof.circleimageview.CircleImageView
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EventoActivity : AppCompatActivity() {
    private var civBack: CircleImageView? = null
    private var atvTopic: AutoCompleteTextView? = null
    private var etContent: TextInputEditText? = null
    private var etDate: TextInputEditText? = null
    private var etHour: TextInputEditText? = null
    private var btnSend: AppCompatButton? = null
    private var auxDate: String? = null
    private var fecha: String? = null
    private var eventPosition: Int? = null
    private var adaptador: AdaptadorTemas? = null
    private var listTema: List<RespuestaTema>? = null
    private lateinit var viewModel: MyViewModel
    private lateinit var d: Dialog
    private var hora = ""
    private var countClic = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)
        viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

        civBack = findViewById(R.id.circleImageViewAgendarBack)
        atvTopic = findViewById(R.id.autoCompleteTextViewTopic)
        etContent = findViewById(R.id.textInpuEditTextContent)
        etDate = findViewById(R.id.textInputEditTextDate)
        etHour = findViewById(R.id.textInputEditTextHour)
        btnSend = findViewById(R.id.buttonSendInfo)

        selectItems()
        scrollEt()

        btnSend?.setOnClickListener {
            sendDataToEvents()
        }

        civBack?.setOnClickListener {
            finish()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun scrollEt() {
        etContent?.setOnTouchListener(OnTouchListener { v1: View, event: MotionEvent ->
            if (v1.id == R.id.textInpuEditTextContent) {
                v1.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> v1.parent
                        .requestDisallowInterceptTouchEvent(false)
                }
            }
            false
        })
    }

    private fun selectItems() {
        listTema = ArrayList()

        if (Network.conexionInternet(MyApp.instance)) {
            viewModel.getAllTemas().observe(this, androidx.lifecycle.Observer {
                listTema = it
                adaptador = AdaptadorTemas(this, it)
                atvTopic?.setAdapter(adaptador)
            })

            atvTopic?.setOnItemClickListener { parent, view, position, id ->
                atvTopic?.setText(listTema?.get(position)?.tema)
                eventPosition = listTema?.get(position)?.id
            }
        } else {
            Toast.makeText(
                this,
                "Requiere una conexión a internet, Por favor vuelva a intentarlo..",
                Toast.LENGTH_SHORT
            ).show()
        }

        etHour?.setOnClickListener {
            if (countClic == 0) {
                dialogHorario()
            }
        }

//        val itemsHour = listOf(
//            "10:00",
//            "11:00",
//            "12:00",
//            "13:00",
//            "14:00",
//            "15:00",
//            "16:00",
//            "17:00"
//        )
//        val adapterHour = ArrayAdapter(applicationContext, R.layout.item_list_select, itemsHour)
//        atvHour?.setAdapter(adapterHour)

        etDate?.setOnClickListener {
            showDatePickerDialog()
        }
    }

    private fun dialogHorario() {
        countClic = 1
        d = Dialog(this@EventoActivity)
        d.requestWindowFeature(Window.FEATURE_NO_TITLE)
        d.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        d.setCancelable(false)
        d.setContentView(R.layout.dialogo_horario_evento)
        d.show()

        var tvCancel:TextView = d.findViewById(R.id.textViewDialogoCancelar)
        var tvAceptar:TextView = d.findViewById(R.id.textViewDialogoAceptar)
        var tvd01:TextView = d.findViewById(R.id.textViewDialogoHora_01)
        var tvd02:TextView = d.findViewById(R.id.textViewDialogoHora_02)
        var tvd03:TextView = d.findViewById(R.id.textViewDialogoHora_03)
        var tvd04:TextView = d.findViewById(R.id.textViewDialogoHora_04)
        var tvd05:TextView = d.findViewById(R.id.textViewDialogoHora_05)
        var tvd06:TextView = d.findViewById(R.id.textViewDialogoHora_06)
        var tvd07:TextView = d.findViewById(R.id.textViewDialogoHora_07)
        var tvd08:TextView = d.findViewById(R.id.textViewDialogoHora_08)
        var rgd:RadioGroup = d.findViewById(R.id.radioGroup)
        var rb_01:RadioButton = d.findViewById(R.id.radioButton_01)
        var rb_02:RadioButton = d.findViewById(R.id.radioButton_02)
        var rb_03:RadioButton = d.findViewById(R.id.radioButton_03)
        var rb_04:RadioButton = d.findViewById(R.id.radioButton_04)
        var rb_05:RadioButton = d.findViewById(R.id.radioButton_05)
        var rb_06:RadioButton = d.findViewById(R.id.radioButton_06)
        var rb_07:RadioButton = d.findViewById(R.id.radioButton_07)
        var rb_08:RadioButton = d.findViewById(R.id.radioButton_08)

        tvd01.setText("10:00 - 11:00 AM")
        tvd02.setText("11:00 - 12:00 PM")
        tvd03.setText("12:00 - 01:00 PM")
        tvd03.isEnabled = false
        tvd03.setTextColor(Color.GRAY)
        tvd04.setText("01:00 - 02:00 PM")
        tvd04.isEnabled = false
        tvd04.setTextColor(Color.GRAY)
        tvd05.setText("02:00 - 03:00 PM")
        tvd05.isEnabled = false
        tvd05.setTextColor(Color.GRAY)
        tvd06.setText("03:00 - 04:00 PM")
        tvd07.setText("04:00 - 05:00 PM")
        tvd08.setText("05:30 - 06:00 PM")

        rb_01.setText("Disponible")
        rb_02.setText("Disponible")
        rb_03.setText("No disponible")
        rb_03.isEnabled = false
        rb_04.setText("No disponible")
        rb_04.isEnabled = false
        rb_05.setText("No disponible")
        rb_05.isEnabled = false
        rb_06.setText("Disponible")
        rb_07.setText("Disponible")
        rb_08.setText("Disponible")

        tvCancel.setOnClickListener {
            countClic = 0
            d.dismiss()
        }

        tvAceptar.setOnClickListener {
            if (rb_01.isChecked) {
                hora = "10:00"
                d.dismiss()
            } else if (rb_02.isChecked) {
                hora = "11:00"
                d.dismiss()
            } else if (rb_03.isChecked) {
                hora = "12:00"
                d.dismiss()
            } else if (rb_04.isChecked) {
                hora = "13:00"
                d.dismiss()
            } else if (rb_05.isChecked) {
                hora = "14:00"
                d.dismiss()
            } else if (rb_06.isChecked) {
                hora = "15:00"
                d.dismiss()
            } else if (rb_07.isChecked) {
                hora = "16:00"
                d.dismiss()
            } else if (rb_08.isChecked) {
                hora = "17:00"
                d.dismiss()
            }

            if (!hora.equals("")) {
                etHour?.setText(hora)
            }

            countClic = 0
            d.dismiss()
        }

    }

    private fun sendDataToEvents() {
        val topic = atvTopic?.text.toString()
        val content = etContent?.text.toString()
        fecha = etDate?.text.toString()
        val date = auxDate
        val hour = etHour?.text.toString()

        if (!topic.isEmpty() && !content.isEmpty() && !date!!.isEmpty() && !hour.isEmpty()) {
            val auxTitle = ""
            noteDialog(auxTitle, content, date, hour, eventPosition!!)
        } else {
            Toast.makeText(this, "Por favor llene todo los campos", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("InlinedApi")
    private fun noteDialog(
        title: String,
        contenido: String,
        fecha: String,
        hora: String,
        numbTema: Int
    ) {
        Log.d("TAG", title)
        val alertDialog: AlertDialog? = this.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle("Enviar cita de evento")
            builder.setMessage(
                "Fecha: ${this.fecha}, Hora: $hora \n\n" +
                        "Nota: Nos contactaremos por mensaje o llamada, para reconfirmar hora y fecha solicitada"
            )
            builder.apply {
                setPositiveButton("Confirmar") { dialog, id ->
                    val reqTeamEvent = SolicitudTemaCita(numbTema)
                    val requestEvent = SolicitudEvento(contenido, fecha, hora, reqTeamEvent)

                    viewModel.insertEvent(requestEvent)
                    finish()

                    dialog.dismiss()
                }
                setNegativeButton("Cancelar") { dialog, id ->
                    dialog.dismiss()
                }
            }
            builder.create()
        }
        alertDialog?.show()
            .apply {
                alertDialog?.getButton(AlertDialog.BUTTON_POSITIVE)?.let {
                    it.setTextColor(BLACK)
                    it.setBackgroundColor(resources.getColor(android.R.color.white))
                }
                alertDialog?.getButton(AlertDialog.BUTTON_NEGATIVE)?.let {
                    it.setTextColor(BLACK)
                    it.setBackgroundColor(resources.getColor(android.R.color.white))
                }
            }
    }

    private fun showDatePickerDialog() {
        val newFragment =
            FechaDialogFragment.newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
                val dayStr = day
                val monthStr = (month + 1)

                val selectedDate = "$year-$monthStr-$dayStr"
                auxDate = selectedDate

                val auxDate: List<String> = selectedDate.split("-")
                val auxYear: String = auxDate[0]
                var auxMonth = ""
                val auxDay: String = auxDate[2]

                when (auxDate[1]) {
                    "1" -> auxMonth = "Enero"
                    "2" -> auxMonth = "Febrero"
                    "3" -> auxMonth = "Marzo"
                    "4" -> auxMonth = "Abril"
                    "5" -> auxMonth = "Mayo"
                    "6" -> auxMonth = "Junio"
                    "7" -> auxMonth = "Julio"
                    "8" -> auxMonth = "Agosto"
                    "9" -> auxMonth = "Septiembre"
                    "10" -> auxMonth = "Octubre"
                    "11" -> auxMonth = "Noviembre"
                    "12" -> auxMonth = "Diciembre"
                }
                val date = "$auxDay de $auxMonth de $auxYear"

                etDate?.setText(date)
            })

        newFragment.show(supportFragmentManager, "datePicker")
    }

    private fun showTimePickerDialog() {
        val cal = Calendar.getInstance()
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            etHour?.setText(SimpleDateFormat("HH:mm a").format(cal.time))
        }
        TimePickerDialog(
            this,
            timeSetListener,
            cal.get(Calendar.HOUR_OF_DAY),
            cal.get(Calendar.MINUTE),
            false
        ).show()
    }

    fun Int.twoDigits() =
        if (this <= 9) "0$this" else this.toString()

}