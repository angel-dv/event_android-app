package app.dm.eventprogram_app.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.dm.eventprogram_app.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
