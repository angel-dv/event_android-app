package app.dm.eventprogram_app.ui

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import app.dm.eventprogram_app.Adaptadores.AdaptadorProyectos
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.proyecto.ProyectoLista
import app.dm.eventprogram_app.estructura.MyViewModel
import app.dm.eventprogram_app.utiles.*

private const val ARG_PARAM1 = "param1"

class HomeFragment : Fragment(), AdapterView.OnItemClickListener {
    private var param1: String? = null
    private lateinit var viewModel: MyViewModel
    private var arrayProyectos: ArrayList<ProyectoLista> = ArrayList()
    private lateinit var lista: ListView
    private lateinit var textoFin: TextView
    private lateinit var adaptador: AdaptadorProyectos

    companion object {
        fun newInstance(param1: String) = HomeFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, param1)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Loading.showProgressDialog(context)
        viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_home, container, false)

        (activity as AppCompatActivity).supportActionBar
        lista = v.findViewById<ListView>(R.id.listaAvances)
        textoFin = v.findViewById<TextView>(R.id.txtFinLista)

        adaptador = AdaptadorProyectos(arrayProyectos, v.context)
        lista.adapter = adaptador
        lista.divider = null
        lista.setOnItemClickListener(this@HomeFragment)

        if (Network.conexionInternet(MyApp.instance)) {

            viewModel.getAllProyectos().observe(viewLifecycleOwner, Observer {
                arrayProyectos.addAll(it)
                //Loading.removeProgressDialog()
                adaptador.setData(it)
                if (arrayProyectos.isEmpty()) textoFin.setText("Aún no tienes proyectos registrados")
            })
        } else {
            Loading.removeProgressDialog()
            Toast.makeText(
                activity,
                "Requiere una conexión a internet,\n Por favor vuelva a intentarlo..",
                Toast.LENGTH_SHORT
            ).show()
        }
        Loading.removeProgressDialog()

        return v
    }


    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        var estaAc: Activity = requireActivity()
        val item = p0?.adapter?.getItem(p2) as ProyectoLista
        val id = item.id
        val color = adaptador.getColorView(p2)
        SharedPreferencesManager.setIntValue(
            Constantes.ID_PROYECTO,
            id
        )
        SharedPreferencesManager.setIntValue(
            Constantes.PROYECTO_COLOR,
            color
        )

        (estaAc as ProyectoS).ProyectoSeleccionado()
    }

}
