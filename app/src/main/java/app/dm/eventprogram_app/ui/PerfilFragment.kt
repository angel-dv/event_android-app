package app.dm.eventprogram_app.ui

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.entidades.usuario.RespuestaDatosUsuario
import app.dm.eventprogram_app.estructura.MyViewModel
import app.dm.eventprogram_app.utiles.Constantes
import app.dm.eventprogram_app.utiles.SharedPreferencesManager
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_profile.*

private const val ARG_PARAM1 = "param1"

class ProfileFragment : Fragment() {
    private var param1: String? = null

    private var cvpFoto: CircleImageView? = null
    private var tvpNombre: TextView? = null
    private var tvpNombreUsuario: TextView? = null
    private var tvpID: TextView? = null
    private var tvpTelefono: TextView? = null
    private var tvpEmail: TextView? = null
    private var tvpEmpresa: TextView? = null
    private var tvpTituloEmpresa: TextView? = null
    private var tvpRegimen: TextView? = null
    private var btnpEditar: Button? = null
    private var btnpCerrarSesion: Button? = null

    private lateinit var viewModel: MyViewModel
    private var rdu: RespuestaDatosUsuario? = null

    private var valueContent: Boolean = true
    private var nombre: String? = null
    private var apellido: String? = null
    private var correo: String? = null
    private var telefono: String? = null
    private var regimen: Int? = null
    private var idUsuario: Int? = null

    companion object {
        fun newInstance(param1: String) = ProfileFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, param1)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_profile, container, false)
        viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

        cvpFoto = v.findViewById(R.id.circleImageViewPerfil)
        tvpNombre = v.findViewById(R.id.textViewPerfilNombre)
        tvpNombreUsuario = v.findViewById(R.id.textViewPerfilNombreUsuario)
        tvpID = v.findViewById(R.id.textViewPerfilID)
        tvpTelefono = v.findViewById(R.id.textViewPerfilTelefono)
        tvpEmail = v.findViewById(R.id.textViewPerfilEmail)
        tvpEmpresa = v.findViewById(R.id.textViewPerfilEmpresa)
        tvpTituloEmpresa = v.findViewById(R.id.textViewPerfilTituloEmpresa)
        tvpRegimen = v.findViewById(R.id.textViewPerfilRegimen)
        btnpEditar = v.findViewById(R.id.buttonPerfilEditar)
        btnpCerrarSesion = v.findViewById(R.id.buttonPerfilCerrarSesion)

        viewModel.getDatosUsuario().observe(viewLifecycleOwner, Observer {
            rdu = RespuestaDatosUsuario(
                it.id,
                it.nombre,
                it.apellido,
                it.telefono,
                it.email,
                it.personaMoral,
                it.rfc,
                it.domicilioFiscal,
                it.password
            )

            SharedPreferencesManager.setIntValue(Constantes.PROFILE_PERSON_ID, rdu?.id)
            SharedPreferencesManager.setStringValue(Constantes.PROFILE_PERSON_NOMBRE, rdu?.nombre)
            SharedPreferencesManager.setStringValue(Constantes.PROFILE_PERSON_APELLIDO, rdu?.apellido)
            SharedPreferencesManager.setStringValue(Constantes.PROFILE_PERSON_CORREO, rdu?.email)
            SharedPreferencesManager.setStringValue(Constantes.PROFILE_PERSON_TELEFONO, rdu?.telefono)
            SharedPreferencesManager.setIntValue(Constantes.PROFILE_PERSON_REGIMEN, rdu?.personaMoral)

            if (valueContent) {
                idUsuario = SharedPreferencesManager.getIntValue(Constantes.PROFILE_PERSON_ID)
                nombre = SharedPreferencesManager.getStringValue(Constantes.PROFILE_PERSON_NOMBRE)
                apellido = SharedPreferencesManager.getStringValue(Constantes.PROFILE_PERSON_APELLIDO)
                correo = SharedPreferencesManager.getStringValue(Constantes.PROFILE_PERSON_CORREO)
                telefono = SharedPreferencesManager.getStringValue(Constantes.PROFILE_PERSON_TELEFONO)
                regimen = SharedPreferencesManager.getIntValue(Constantes.PROFILE_PERSON_REGIMEN)

                if (apellido == null) {
                    apellido = ""
                }

                if (regimen == 0) {
                    tvpRegimen?.setText("Persona Fisica")
                    tvpEmpresa?.visibility = View.GONE
                    tvpTituloEmpresa?.visibility = View.GONE
                } else if (regimen == 1) {
                    tvpEmpresa?.visibility = View.VISIBLE
                    tvpTituloEmpresa?.visibility = View.VISIBLE

                    tvpEmpresa?.setText("Inblex software")
                    tvpRegimen?.setText("Persona Moral")
                } else {
                    tvpTituloEmpresa?.visibility = View.GONE
                    tvpEmpresa?.visibility = View.GONE
                }

                tvpID?.setText(idUsuario.toString())
                tvpNombre?.setText("$nombre $apellido")
                tvpNombreUsuario?.setText("$nombre $apellido")
                tvpEmail?.setText(correo)
                tvpTelefono?.setText(telefono)

                valueContent = false
            }
        })

        idUsuario = SharedPreferencesManager.getIntValue(Constantes.PROFILE_PERSON_ID)
        nombre = SharedPreferencesManager.getStringValue(Constantes.PROFILE_PERSON_NOMBRE)
        apellido = SharedPreferencesManager.getStringValue(Constantes.PROFILE_PERSON_APELLIDO)
        correo = SharedPreferencesManager.getStringValue(Constantes.PROFILE_PERSON_CORREO)
        telefono = SharedPreferencesManager.getStringValue(Constantes.PROFILE_PERSON_TELEFONO)
        regimen = SharedPreferencesManager.getIntValue(Constantes.PROFILE_PERSON_REGIMEN)

        if (apellido == null) {
            apellido = ""
        }

        tvpNombre?.setText("$nombre $apellido")
        tvpNombreUsuario?.setText("$nombre $apellido")
        tvpID?.setText(idUsuario.toString())
        tvpEmail?.setText(correo)
        tvpTelefono?.setText(telefono)

        if (regimen == 0) {
            tvpRegimen?.setText("Persona Fisica")
            tvpEmpresa?.visibility = View.GONE
            tvpTituloEmpresa?.visibility = View.GONE
        } else if (regimen == 1) {
            tvpEmpresa?.visibility = View.VISIBLE
            tvpTituloEmpresa?.visibility = View.VISIBLE

            tvpEmpresa?.setText("Inblex software")
            tvpRegimen?.setText("Persona Moral")
        } else {
            tvpTituloEmpresa?.visibility = View.GONE
            tvpEmpresa?.visibility = View.GONE
        }

        btnpCerrarSesion?.setOnClickListener {
            logOut()
        }

        btnpEditar?.setOnClickListener {
            Toast.makeText(context, "Editar en produccion", Toast.LENGTH_SHORT).show()
        }

        return v
    }

    private fun logOut() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle("Cerrar sesión")
            builder.setMessage("¿Desea cerrar su sesión?")
            builder.apply {
                setPositiveButton("Aceptar") { dialog, id ->
                    //Elimina los datos almacenados del usuario del -> Inicio de Sesion(Login)
                    SharedPreferencesManager.removeDataValue(Constantes.LOGIN_PASS)
                    SharedPreferencesManager.removeDataValue(Constantes.LOGIN_EMAIL)
                    SharedPreferencesManager.removeDataValue(Constantes.LOGIN_USERNAME)
                    SharedPreferencesManager.removeDataValue(Constantes.LOGIN_ID)
                    SharedPreferencesManager.removeDataValue(Constantes.LOGIN_TOKEN)

                    SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_ID)
                    SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_NOMBRE)
                    SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_CORREO)
                    SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_TELEFONO)
                    SharedPreferencesManager.removeDataValue(Constantes.PROFILE_PERSON_REGIMEN)

                    SharedPreferencesManager.removeDataValue(Constantes.REFRESH_TOKEN)

                    val i = Intent(activity, InicioSesion::class.java)
                    startActivity(i)
                    activity!!.finish()
                    dialog.dismiss()
                }
                setNegativeButton("Cancelar") { dialog, id ->
                    dialog.dismiss()
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
