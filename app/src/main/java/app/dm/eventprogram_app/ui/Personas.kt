package app.dm.eventprogram_app.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import app.dm.eventprogram_app.R
import app.dm.eventprogram_app.api.ApiUsuarioCliente
import app.dm.eventprogram_app.api.ApiUsuarioServicio
import app.dm.eventprogram_app.entidades.registro.RespuestaRegistro
import app.dm.eventprogram_app.entidades.registro.SolicitudRegistro
import app.dm.eventprogram_app.utiles.Loading
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Personas : Fragment() {
    private var apiUsuarioCliente: ApiUsuarioCliente? = null
    private var apiUsuarioServicio: ApiUsuarioServicio? = null
    private var etpNombre: EditText? = null
    private var etpApellido: EditText? = null
    private var etpRFC: EditText? = null
    private var etpCorreo: EditText? = null
    private var etpDomicilio: EditText? = null
    private var etpTelefono: EditText? = null
    private var etpContrasenia: EditText? = null
    private var etpConfContrasenia: EditText? = null
    private var btnpRegistrar: Button? = null
    private var tvInicioSesion: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_personas, container, false)
        initRetrofit()
        findViews(v)

        btnpRegistrar?.setOnClickListener {
            val nombre: String = etpNombre?.text.toString()
            val apellido = etpApellido?.text.toString()
            var rfc = etpRFC?.text.toString()
            val correo = etpCorreo?.text.toString()
            var domicilio = etpDomicilio?.text.toString()
            val telefono = etpTelefono?.text.toString()
            val contrasenia = etpContrasenia?.text.toString()
            val confContrasenia = etpConfContrasenia?.text.toString()
            if (nombre.isNotEmpty() && apellido.isNotEmpty() && telefono.isNotEmpty() && correo.isNotEmpty() && contrasenia.isNotEmpty() && confContrasenia.isNotEmpty()) {
                if (contrasenia.equals(confContrasenia)) {
                    if (rfc.isEmpty()) rfc = ""
                    if (domicilio.isEmpty()) domicilio = ""
                    registrarPersona(
                        nombre,
                        apellido,
                        telefono,
                        correo,
                        rfc,
                        domicilio,
                        contrasenia
                    )
                } else {
                    etpConfContrasenia?.setError("Las contraseñas no coinciden")
                }
            } else {
                if (nombre.isEmpty()) etpNombre?.setError("Su nombre es necesario")
                if (apellido.isEmpty()) etpApellido?.setError("Su apellido es necesario")
                if (telefono.isEmpty()) etpTelefono?.setError("Su teléfono es necesario")
                if (correo.isEmpty()) etpCorreo?.setError("Su correo es necesario")
                if (contrasenia.isEmpty()) etpContrasenia?.setError("Por favor, escriba una contraseña")
                if (confContrasenia.isEmpty()) etpConfContrasenia?.setError("Por favor, confirme su contraseña")
            }

        }

        tvInicioSesion?.setOnClickListener {
            activity!!.finish()
        }

        return v
    }

    private fun findViews(v: View) {
        etpNombre = v.findViewById(R.id.editTextPersonaNombre)
        etpApellido = v.findViewById(R.id.editTextPersonaApellido)
        etpRFC = v.findViewById(R.id.editTextPersonaRFC)
        etpCorreo = v.findViewById(R.id.editTextPersonaCorreo)
        etpDomicilio = v.findViewById(R.id.editTextPersonaDomicilio)
        etpTelefono = v.findViewById(R.id.editTextPersonaTelefono)
        etpContrasenia = v.findViewById(R.id.editTextPersonaContrasenia)
        etpConfContrasenia = v.findViewById(R.id.editTextPersonaConfContrasenia)
        btnpRegistrar = v.findViewById(R.id.buttonPersonaRegistrar)
        tvInicioSesion = v.findViewById(R.id.textViewPersonaIniciarSesion)
    }

    private fun initRetrofit() {
        apiUsuarioCliente = ApiUsuarioCliente.instance
        apiUsuarioServicio = apiUsuarioCliente?.getApiUsuarioServicio()
    }

    private fun registrarPersona(
        nombre: String,
        apellido: String,
        telefono: String,
        correo: String,
        rfc: String,
        domicilio: String,
        contra: String
    ) {

        Loading.showProgressDialog(context)
        val registro =
            SolicitudRegistro(nombre, apellido, telefono, correo, 0, rfc, domicilio, contra)

        val call: Call<RespuestaRegistro>? = apiUsuarioServicio?.registro(registro)

        call?.enqueue(object : Callback<RespuestaRegistro> {
            override fun onFailure(call: Call<RespuestaRegistro>, t: Throwable) {
                Log.d("TAG-registro", t.message.toString())
                Toast.makeText(
                    context,
                    "Error al conectar con el servidor\nPor favor intente mas tarde",
                    Toast.LENGTH_LONG
                ).show()
                Loading.removeProgressDialog()

            }

            override fun onResponse(
                call: Call<RespuestaRegistro>,
                response: Response<RespuestaRegistro>
            ) {
                if (response.isSuccessful) {
                    Toast.makeText(
                        context,
                        "Persona registrada",
                        Toast.LENGTH_SHORT
                    ).show()
                    Loading.removeProgressDialog()
                    vaciar()
                } else {
                    Log.d("TAG_registro", response.body().toString())
                    Loading.removeProgressDialog()
                    Toast.makeText(
                        context,
                        "Lo sentimos, ha habido un problema con su registro, intente mas tarde",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    fun vaciar() {
        etpNombre?.setText("")
        etpApellido?.setText("")
        etpRFC?.setText("")
        etpCorreo?.setText("")
        etpDomicilio?.setText("")
        etpTelefono?.setText("")
        etpContrasenia?.setText("")
        etpConfContrasenia?.setText("")
        btnpRegistrar?.setText("")
    }
}
