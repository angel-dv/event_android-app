package app.dm.eventprogram_app.ui

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.TextView
import app.dm.eventprogram_app.R

class DialogoDetalleEventos {

    private var tvdTitulo: TextView
    private var tvdFecha: TextView
    private var tvdHora: TextView
    private var tvdAsunto: TextView
    private var tvdEstado: TextView
    private var tvdAceptar: TextView
    private var tvdPosponer: TextView
    private var d: Dialog

    constructor(contexto: Context) {
        d = Dialog(contexto)
        d.requestWindowFeature(Window.FEATURE_NO_TITLE)
        d.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        d.setCancelable(false)
        d.setContentView(R.layout.dialogo_detalles_evento)

        tvdTitulo = d.findViewById(R.id.textViewDialogoTitulo)
        tvdFecha = d.findViewById(R.id.textViewDialogoFecha)
        tvdHora = d.findViewById(R.id.textViewDialogoHora)
        tvdAsunto = d.findViewById(R.id.textViewDialogoAsunto)
        tvdEstado = d.findViewById(R.id.textViewDialogoEstado)
        tvdAceptar = d.findViewById(R.id.textViewDialogoAceptar)
        tvdPosponer = d.findViewById(R.id.textViewDialogoPosponer)

        tvdAceptar.setOnClickListener {
            DialogoClics.onClickDialog(1)
            d.dismiss()
        }

        tvdPosponer.setOnClickListener {
            DialogoClics.onClickDialog(1)
            d.dismiss()
        }
    }

    fun setDatosEvento(
        titulo: String,
        fecha: String,
        hora: String,
        asunto: String,
        estado: String
    ) {
        tvdTitulo.setText(titulo)
        tvdFecha.setText(fecha)
        tvdHora.setText(hora.substring(0, 5))
        tvdAsunto.setText(asunto)
        tvdEstado.setText(estado)
    }

    fun mostrarDialogoEvento() {
        d.show()
    }

}